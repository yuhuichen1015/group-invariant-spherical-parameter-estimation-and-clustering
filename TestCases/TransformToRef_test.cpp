
#include <iostream>
#include <iterator>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <time.h>
#include <math.h>

#include <TransformToRef.h>
#include <TransformToRef_types.h>

#include "RWMtx.h"

using namespace::std;

int main(int argc, char *argv[]){
    if(argc!=4){
        cout << "Usage: TransformToRef <QuatRef.mtx> <Quat.mtx> <Pm.mtx>" << endl;
        return 0;
    }

    emxArray_real_T* QuatRef = ReadMtx2emxArray_real_T(argv[1]);
    emxArray_real_T* Quat = ReadMtx2emxArray_real_T(argv[2]);
    emxArray_real_T* Pm = ReadMtx2emxArray_real_T(argv[3]);
    emxArray_Print_real_T(Quat);
    emxArray_Print_real_T(QuatRef);
    // Initial output
    emxArray_real_T* QuatT = emxArray_InitBySize_real_T(Quat->size[0], Quat->size[1]);
    // emxArray_real_T* QuatT;
    cout << Quat->size[0] << " " << Quat->size[1] << endl;

    TransformToRef(QuatRef, Quat, Pm, QuatT);

    emxArray_Print_real_T(QuatT);
    return 0;
}
