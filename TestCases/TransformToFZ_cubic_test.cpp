
#include <iostream>
#include <iterator>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <time.h>
#include <math.h>

#include <TransformToFZ_cubic.h>
#include <TransformToFZ_cubic_types.h>

#include "RWMtx.h"

using namespace::std;

int main(int argc, char *argv[]){
    if(argc!=2){
        cout << "Usage: TransformToFZ_cubic <Quat.mtx>" << endl;
        return 0;
    }

    emxArray_real_T* Quat = ReadMtx2emxArray_real_T(argv[1]);
    emxArray_Print_real_T(Quat);

    emxArray_real_T* QuatT = emxArray_InitBySize_real_T(Quat->size[0], Quat->size[1]);   

    TransformToFZ_cubic(Quat, QuatT);
    emxArray_Print_real_T(QuatT);

    return 0;
}
