
#include <iostream>
#include <iterator>
#include <cstdlib>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <time.h>
#include <math.h>

#include <EMmWatson.h>
#include <EMmWatson_types.h>

#include "RWMtx.h"

using namespace::std;

int main(int argc, char *argv[]){
    if(argc!=4){
        cout << "Usage: EMmWatson <Samples.mtx> <Pm.mtx> <Num_of_clusters>" << endl;
        return 0;
    }

    emxArray_real_T* Samples = ReadMtx2emxArray_real_T(argv[1]);
    emxArray_real_T* Pm = ReadMtx2emxArray_real_T(argv[2]);
    int Num_of_clusters = atoi(argv[3]);

    cout << "Number of clusters:" << Num_of_clusters << endl;


    // Initial output
    emxArray_real_T* Mu_est = emxArray_InitBySize_real_T(Num_of_clusters, 4);
    emxArray_real_T* Kappa_est = emxArray_InitBySize_real_T(Num_of_clusters, 1);
    emxArray_real_T* W_est = emxArray_InitBySize_real_T(Num_of_clusters, 1);
    emxArray_real_T* CIdx = emxArray_InitBySize_real_T(Samples->size[0], 1);
    double logL;

    EMmWatson(Samples, Pm, double(Num_of_clusters), Mu_est, Kappa_est, W_est, &logL, CIdx);
    
    cout << "Mu_est:" << endl;
    emxArray_Print_real_T(Mu_est);

    cout << "Kappa_est:" << endl;
    emxArray_Print_real_T(Kappa_est);

    cout << "W_est:" << endl;
    emxArray_Print_real_T(W_est);

    double cluster1=0;
    cout << "CIdx size:" << CIdx->size[0] << " " << CIdx->size[1] << endl;
    for(int i=0; i<CIdx->size[0]; i++) {
        if(CIdx->data[i]==1) {
            cluster1 += 1.0/CIdx->size[0];
        }
    }
    cout << "First cluster:" << cluster1 << endl;
    return 0;
}
