#ifndef __RWMTX_H__
#define __RWMTX_H__
/* Include files */
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <iomanip>
#include <TransformToRef_types.h>
#include <TransformToRef_emxutil.h>

using namespace::std;

emxArray_real_T* ReadMtx2emxArray_real_T(const char* filename){
    cout << "Opening file:" << filename << endl;
    FILE* pFile;
    // Open file
    pFile = fopen(filename, "rb");
    if(pFile==NULL){
        fputs("File error\n", stderr);
        exit(1);
    }
    
    //Obtain the size of the matrix
    int row, col, page;
    fscanf(pFile, "%d", &row);
    fscanf(pFile, "%d", &col);
    fscanf(pFile, "%d", &page);
    char c = fgetc(pFile);
    
    emxArray_real_T* I;
    if(page==1) {
      emxInit_real_T(&I, 2);
      I->size[0] = row;
      I->size[1] = col;
    } else {
      emxInit_real_T(&I, 3);
      I->size[0] = row;
      I->size[1] = col;
      I->size[2] = page;
    }
    emxEnsureCapacity((emxArray__common *)I, row*col*page, (int32_T)sizeof(real_T));
    double buff;
    for(int i=0; i<row*col*page; i++){
      fread(&buff, sizeof(double), 1, pFile);
      I->data[i] = buff;
    }
    return I;
}

// emxArray_boolean_T* ReadMtx2emxArray_boolean_T(const char* filename){
//     FILE* pFile;
//     // Open file
//     pFile = fopen(filename, "rb");
//     if(pFile==NULL){
//         fputs("File error", stderr);
//         exit(1);
//     }
    
//     //Obtain the size of the matrix
//     int row, col;
//     fscanf(pFile, "%d", &row);
//     fscanf(pFile, "%d", &col);
//     char c = fgetc(pFile);
    
//     emxArray_boolean_T* I;
//     emxInit_boolean_T(&I, 2);
//     I->size[0] = col;
//     I->size[1] = row;
//     emxEnsureCapacity((emxArray__common *)I, row*col, (int32_T)sizeof(boolean_T));
//     boolean_T buff;
//     for(int i=0; i<row*col; i++){
//       fread(&buff, sizeof(boolean_T), 1, pFile);
//       I->data[i] = buff;        
//     }
//     return I;
// }

emxArray_real_T* emxArray_InitBySize_real_T(int sz0, int sz1){
  emxArray_real_T* I;    
  emxInit_real_T(&I, 2);
  I->size[0] = sz0;
  I->size[1] = sz1;
  emxEnsureCapacity((emxArray__common *)I, sz0*sz1, (int32_T)sizeof(real_T));
  return I;
}

void WriteemxArray2Mtx_real_T(const char* filename, emxArray_real_T* I){
    FILE* pFile;
    int row, col, page;
    if(I->numDimensions==2){
      row = I->size[0];
      col = I->size[1];
      page = 1;
    } else {
      row = I->size[0];
      col = I->size[1];
      page = I->size[2];
    }
    // Open file
    pFile = fopen(filename, "w");
    // Write the header
    fprintf(pFile, "%d %d %d\n", row, col, page);

    //Write the content
    for(int p=0; p<page; p++){
      for(int i=0; i<col*row; i++){
        fwrite(&(I->data[i+p*col*row]), sizeof(double), 1, pFile);
      }
    }
    fclose(pFile);
}

void emxArray_Print_real_T(emxArray_real_T* I){
  int page;
  if(I->numDimensions==2) {
    cout << "Matrix dimension: " << I->size[0] << "x" << I->size[1] << endl;
    page = 1;
  } else {
    cout << "Matrix dimension: " << I->size[0] << "x" << I->size[1] << "x" << I->size[2] << endl;
    page = I->size[2];
  }
  for(int p=0; p<page; p++) {
    cout << "page: " << p << endl;
    for(int i=0; i<I->size[0]; i++){
      for(int j=0; j<I->size[1]; j++){
        cout << setiosflags(ios::fixed) << setprecision(2) << I->data[i+j*I->size[0]+p*I->size[0]*I->size[1]] << "\t";
      }
      cout << endl;
    }  
  }
}

// void emxArray_Print_boolean_T(emxArray_boolean_T* I){
//   cout << "Matrix dimension: " << I->size[1] << "X" << I->size[0] << endl;    
//   for(int i=0; i<I->size[1]; i++){
//     for(int j=0; j<I->size[0]; j++){
//       if(I->data[j+i*I->size[0]]==true){
//         cout << 1 << " ";
//       }
//       else{
//         cout << 0 << " ";
//       }
//     }
//     cout << endl;
//   }  
// }


#endif
