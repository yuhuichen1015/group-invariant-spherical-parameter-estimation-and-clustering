
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <math.h>

#include <TransformToRef.h>
#include <TransformToRef_types.h>

#include "RWMtx.h"

using namespace::std;

int main(int argc, char *argv[]){
    if(argc!=2){
        cout << "Usage: RWMtx_test <-X.mtx>" << endl;
        return 0;
    }

    // Read the .mtx file
    emxArray_real_T* Samples = ReadMtx2emxArray_real_T(argv[1]);

    // Print the mtx file
    emxArray_Print_real_T(Samples);
    
    return 0;
}
