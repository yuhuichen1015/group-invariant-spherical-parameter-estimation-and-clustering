//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: repmat.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:04:28
//

// Include Files
#include "rt_nonfinite.h"
#include "EMmVMF.h"
#include "repmat.h"
#include "EMmVMF_emxutil.h"

// Function Definitions

//
// Arguments    : const emxArray_real_T *a
//                const double varargin_1[2]
//                emxArray_real_T *b
// Return Type  : void
//
void b_repmat(const emxArray_real_T *a, const double varargin_1[2],
              emxArray_real_T *b)
{
  int outsize_idx_0;
  int jtilecol;
  boolean_T p;
  int ibtile;
  int k;
  outsize_idx_0 = a->size[0];
  jtilecol = b->size[0] * b->size[1];
  b->size[0] = a->size[0];
  b->size[1] = (int)varargin_1[1];
  emxEnsureCapacity((emxArray__common *)b, jtilecol, (int)sizeof(double));
  if (!(a->size[0] == 0)) {
    if (outsize_idx_0 == 0) {
      p = true;
    } else if ((int)varargin_1[1] == 0) {
      p = true;
    } else {
      p = false;
    }

    if (!p) {
      outsize_idx_0 = a->size[0];
      for (jtilecol = 1; jtilecol <= (int)varargin_1[1]; jtilecol++) {
        ibtile = (jtilecol - 1) * outsize_idx_0;
        for (k = 1; k <= outsize_idx_0; k++) {
          b->data[(ibtile + k) - 1] = a->data[k - 1];
        }
      }
    }
  }
}

//
// Arguments    : const emxArray_real_T *a
//                emxArray_real_T *b
// Return Type  : void
//
void c_repmat(const emxArray_real_T *a, emxArray_real_T *b)
{
  int outsize_idx_0;
  int jtilecol;
  int ibtile;
  int k;
  outsize_idx_0 = a->size[0];
  jtilecol = b->size[0] * b->size[1];
  b->size[0] = a->size[0];
  b->size[1] = 4;
  emxEnsureCapacity((emxArray__common *)b, jtilecol, (int)sizeof(double));
  if ((!(a->size[0] == 0)) && (!(outsize_idx_0 == 0))) {
    outsize_idx_0 = a->size[0];
    for (jtilecol = 0; jtilecol < 4; jtilecol++) {
      ibtile = jtilecol * outsize_idx_0;
      for (k = 1; k <= outsize_idx_0; k++) {
        b->data[(ibtile + k) - 1] = a->data[k - 1];
      }
    }
  }
}

//
// Arguments    : const emxArray_real_T *a
//                const double varargin_1[3]
//                emxArray_real_T *b
// Return Type  : void
//
void repmat(const emxArray_real_T *a, const double varargin_1[3],
            emxArray_real_T *b)
{
  int outsize_idx_0;
  int ibtile;
  boolean_T p;
  int nrows;
  int ntilecols;
  int k;
  int iv0[3];
  outsize_idx_0 = a->size[0];
  ibtile = b->size[0] * b->size[1] * b->size[2];
  b->size[0] = a->size[0];
  b->size[1] = (int)varargin_1[1];
  b->size[2] = (int)varargin_1[2];
  emxEnsureCapacity((emxArray__common *)b, ibtile, (int)sizeof(double));
  if (!(a->size[0] == 0)) {
    if (outsize_idx_0 == 0) {
      p = true;
    } else if ((int)varargin_1[1] == 0) {
      p = true;
    } else if ((int)varargin_1[2] == 0) {
      p = true;
    } else {
      p = false;
    }

    if (!p) {
      nrows = a->size[0];
      ntilecols = 1;
      k = 3;
      while ((k > 2) && ((int)varargin_1[2] == 1)) {
        k = 2;
      }

      for (outsize_idx_0 = 2; outsize_idx_0 <= k; outsize_idx_0++) {
        iv0[0] = 1;
        iv0[1] = (int)varargin_1[1];
        iv0[2] = (int)varargin_1[2];
        ntilecols *= iv0[outsize_idx_0 - 1];
      }

      for (outsize_idx_0 = 1; outsize_idx_0 <= ntilecols; outsize_idx_0++) {
        ibtile = (outsize_idx_0 - 1) * nrows;
        for (k = 1; k <= nrows; k++) {
          b->data[(ibtile + k) - 1] = a->data[k - 1];
        }
      }
    }
  }
}

//
// File trailer for repmat.cpp
//
// [EOF]
//
