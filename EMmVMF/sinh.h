//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: sinh.h
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:04:28
//
#ifndef __SINH_H__
#define __SINH_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "EMmVMF_types.h"

// Function Declarations
extern void b_sinh(emxArray_real_T *x);

#endif

//
// File trailer for sinh.h
//
// [EOF]
//
