//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: rdivide.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:04:28
//

// Include Files
#include "rt_nonfinite.h"
#include "EMmVMF.h"
#include "rdivide.h"
#include "EMmVMF_emxutil.h"

// Function Definitions

//
// Arguments    : const emxArray_real_T *x
//                const emxArray_real_T *y
//                emxArray_real_T *z
// Return Type  : void
//
void b_rdivide(const emxArray_real_T *x, const emxArray_real_T *y,
               emxArray_real_T *z)
{
  int i2;
  int loop_ub;
  i2 = z->size[0] * z->size[1];
  z->size[0] = x->size[0];
  z->size[1] = x->size[1];
  emxEnsureCapacity((emxArray__common *)z, i2, (int)sizeof(double));
  loop_ub = x->size[0] * x->size[1];
  for (i2 = 0; i2 < loop_ub; i2++) {
    z->data[i2] = x->data[i2] / y->data[i2];
  }
}

//
// Arguments    : const emxArray_real_T *x
//                const emxArray_real_T *y
//                emxArray_real_T *z
// Return Type  : void
//
void rdivide(const emxArray_real_T *x, const emxArray_real_T *y, emxArray_real_T
             *z)
{
  int i0;
  int loop_ub;
  i0 = z->size[0] * z->size[1] * z->size[2];
  z->size[0] = x->size[0];
  z->size[1] = x->size[1];
  z->size[2] = x->size[2];
  emxEnsureCapacity((emxArray__common *)z, i0, (int)sizeof(double));
  loop_ub = x->size[0] * x->size[1] * x->size[2];
  for (i0 = 0; i0 < loop_ub; i0++) {
    z->data[i0] = x->data[i0] / y->data[i0];
  }
}

//
// File trailer for rdivide.cpp
//
// [EOF]
//
