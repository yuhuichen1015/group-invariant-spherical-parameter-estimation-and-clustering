//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: EMmVMF.h
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:04:28
//
#ifndef __EMMVMF_H__
#define __EMMVMF_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "EMmVMF_types.h"

// Function Declarations
extern void EMmVMF(const emxArray_real_T *X, const emxArray_real_T *Pm, double
                   Num_of_clusters, emxArray_real_T *Mu, emxArray_real_T *Kappa,
                   emxArray_real_T *W, double *logL, emxArray_real_T *CIdx);

#endif

//
// File trailer for EMmVMF.h
//
// [EOF]
//
