//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: cosh.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:04:28
//

// Include Files
#include "rt_nonfinite.h"
#include "EMmVMF.h"
#include "cosh.h"

// Function Definitions

//
// Arguments    : emxArray_real_T *x
// Return Type  : void
//
void b_cosh(emxArray_real_T *x)
{
  int i3;
  int k;
  i3 = x->size[0];
  for (k = 0; k < i3; k++) {
    x->data[k] = cosh(x->data[k]);
  }
}

//
// File trailer for cosh.cpp
//
// [EOF]
//
