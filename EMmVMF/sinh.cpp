//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: sinh.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:04:28
//

// Include Files
#include "rt_nonfinite.h"
#include "EMmVMF.h"
#include "sinh.h"

// Function Definitions

//
// Arguments    : emxArray_real_T *x
// Return Type  : void
//
void b_sinh(emxArray_real_T *x)
{
  int i4;
  int k;
  i4 = x->size[0];
  for (k = 0; k < i4; k++) {
    x->data[k] = sinh(x->data[k]);
  }
}

//
// File trailer for sinh.cpp
//
// [EOF]
//
