//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: mybesseli.h
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 28-Jul-2015 16:26:21
//
#ifndef __MYBESSELI_H__
#define __MYBESSELI_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "EMmVMF_types.h"

// Function Declarations
extern double mybesseli(double x);

#endif

//
// File trailer for mybesseli.h
//
// [EOF]
//
