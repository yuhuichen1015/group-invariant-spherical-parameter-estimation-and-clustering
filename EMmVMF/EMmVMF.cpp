//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: EMmVMF.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:04:28
//

// Include Files
#include "rt_nonfinite.h"
#include "EMmVMF.h"
#include "norm.h"
#include "randn.h"
#include "sinh.h"
#include "Cp.h"
#include "EMmVMF_emxutil.h"
#include "cosh.h"
#include "sum.h"
#include "repmat.h"
#include "invAp.h"
#include "rdivide.h"
#include "squeeze.h"

// Function Declarations
static int div_nzp_s32_floor(int numerator, int denominator);

// Function Definitions

//
// Arguments    : int numerator
//                int denominator
// Return Type  : int
//
static int div_nzp_s32_floor(int numerator, int denominator)
{
  int quotient;
  unsigned int absNumerator;
  unsigned int absDenominator;
  boolean_T quotientNeedsNegation;
  unsigned int tempAbsQuotient;
  if (numerator >= 0) {
    absNumerator = (unsigned int)numerator;
  } else {
    absNumerator = (unsigned int)-numerator;
  }

  if (denominator >= 0) {
    absDenominator = (unsigned int)denominator;
  } else {
    absDenominator = (unsigned int)-denominator;
  }

  quotientNeedsNegation = ((numerator < 0) != (denominator < 0));
  tempAbsQuotient = absNumerator / absDenominator;
  if (quotientNeedsNegation) {
    absNumerator %= absDenominator;
    if (absNumerator > 0U) {
      tempAbsQuotient++;
    }
  }

  if (quotientNeedsNegation) {
    quotient = -(int)tempAbsQuotient;
  } else {
    quotient = (int)tempAbsQuotient;
  }

  return quotient;
}

//
// This is the EM algorithm for estimating the parameters of group-invariant
//  von Mises-Fisher distribution. It also supports the clustering of
//  group-invariant samples. Details of the algorithm can be found in the
//  following paper:
//    Yu-Hui Chen, Dennis Wei, Gregory Newstadt, Marc DeGraef,
//    Jeffrey Simmons, Alfred Hero "Statistical Estimation and Clustering of
//    Group-invariant Orientation Parameters" *International Conference on
//    Information Fusion, 2015
//
//  Usage:
//   [Mu, Kappa, W, logL, CIdx]=EMmVMF(X, Pm, Num_of_clusters(optional))
//
//  Inputs:
//    X : A N x 4 matrix which contains the N quaternion samples which need to
//        be clustered or estimated its mean and concentration.
//
//    Pm : A 4 x 4 x M matrx where each of the 4 x 4 submatrix is the
//        symmetry operator in rotation orthnormal matrix form.
//
//    Num_of_clusters : An integer which specify the number of clusters in
//        the samples.
//
//
//  Outputs:
//    Mu : A Num_of_clusters x 4 matrix which contains the estimated means of
//        each cluster in quaternion.
//
//    Kappa : A Num_of_clusters x 1 matrix which contains the estimated kappa
//        of each cluster.
//
//    W : A Num_of_clusters x 1 matrix which contains the mixing weight of
//        the clusters.
//
//    logL : The log-likelihood for the samples of the estimated model.
//
//    CIdx : The N x 1 matrix which indicates the cluster of each sample it
//        belongs to.
//
//  Function is written by Yu-Hui Chen, University of Michigan
// Arguments    : const emxArray_real_T *X
//                const emxArray_real_T *Pm
//                double Num_of_clusters
//                emxArray_real_T *Mu
//                emxArray_real_T *Kappa
//                emxArray_real_T *W
//                double *logL
//                emxArray_real_T *CIdx
// Return Type  : void
//
void EMmVMF(const emxArray_real_T *X, const emxArray_real_T *Pm, double
            Num_of_clusters, emxArray_real_T *Mu, emxArray_real_T *Kappa,
            emxArray_real_T *W, double *logL, emxArray_real_T *CIdx)
{
  emxArray_real_T *Mu_All;
  int No;
  int N;
  int ixstop;
  int iy;
  emxArray_real_T *Kappa_All;
  emxArray_real_T *W_All;
  double L_All[5];
  int ixstart;
  emxArray_real_T *CosHSum_All;
  emxArray_real_T *SinH;
  emxArray_real_T *CosH;
  emxArray_real_T *R;
  emxArray_real_T *CosHSum;
  emxArray_real_T *CosHSumNorm;
  emxArray_real_T *tmpGamma;
  emxArray_real_T *r0;
  emxArray_real_T *x;
  emxArray_real_T *b_x;
  emxArray_real_T *y;
  emxArray_real_T *b;
  emxArray_real_T *r1;
  emxArray_real_T *r2;
  emxArray_real_T *r3;
  emxArray_real_T *r4;
  emxArray_real_T *b_SinH;
  emxArray_real_T *b_y;
  emxArray_real_T *b_R;
  int itmp;
  int ix;
  double Gamma[4];
  double mtmp;
  double L[30];
  int ite;
  boolean_T exitg3;
  int j;
  double c_y[4];
  int n;
  unsigned int unnamed_idx_0;
  int b_ix;
  int cindx;
  int vstride;
  double dv0[3];
  double dv1[2];
  int b_W[1];
  emxArray_real_T c_W;
  double a[16];
  double dv2[4];
  boolean_T exitg2;
  unsigned int outsz[2];
  emxArray_int32_T *iindx;
  boolean_T exitg1;
  emxInit_real_T(&Mu_All, 3);

  // %% Duplicate the Euler Angles
  No = Pm->size[2];
  N = X->size[0];

  //  Precompute the xAp, yAp for invAp_fast
  //  Create container for estimated parameters
  ixstop = Mu_All->size[0] * Mu_All->size[1] * Mu_All->size[2];
  Mu_All->size[0] = (int)Num_of_clusters;
  Mu_All->size[1] = 4;
  Mu_All->size[2] = 5;
  emxEnsureCapacity((emxArray__common *)Mu_All, ixstop, (int)sizeof(double));
  iy = ((int)Num_of_clusters << 2) * 5;
  for (ixstop = 0; ixstop < iy; ixstop++) {
    Mu_All->data[ixstop] = 0.0;
  }

  b_emxInit_real_T(&Kappa_All, 2);
  ixstop = Kappa_All->size[0] * Kappa_All->size[1];
  Kappa_All->size[0] = (int)Num_of_clusters;
  Kappa_All->size[1] = 5;
  emxEnsureCapacity((emxArray__common *)Kappa_All, ixstop, (int)sizeof(double));
  iy = (int)Num_of_clusters * 5;
  for (ixstop = 0; ixstop < iy; ixstop++) {
    Kappa_All->data[ixstop] = 0.0;
  }

  b_emxInit_real_T(&W_All, 2);
  ixstop = W_All->size[0] * W_All->size[1];
  W_All->size[0] = (int)Num_of_clusters;
  W_All->size[1] = 5;
  emxEnsureCapacity((emxArray__common *)W_All, ixstop, (int)sizeof(double));
  iy = (int)Num_of_clusters * 5;
  for (ixstop = 0; ixstop < iy; ixstop++) {
    W_All->data[ixstop] = 0.0;
  }

  for (ixstart = 0; ixstart < 5; ixstart++) {
    L_All[ixstart] = 0.0;
  }

  emxInit_real_T(&CosHSum_All, 3);
  ixstart = X->size[0];
  ixstop = CosHSum_All->size[0] * CosHSum_All->size[1] * CosHSum_All->size[2];
  CosHSum_All->size[0] = ixstart;
  emxEnsureCapacity((emxArray__common *)CosHSum_All, ixstop, (int)sizeof(double));
  ixstop = CosHSum_All->size[0] * CosHSum_All->size[1] * CosHSum_All->size[2];
  CosHSum_All->size[1] = (int)Num_of_clusters;
  CosHSum_All->size[2] = 5;
  emxEnsureCapacity((emxArray__common *)CosHSum_All, ixstop, (int)sizeof(double));
  iy = X->size[0] * (int)Num_of_clusters * 5;
  for (ixstop = 0; ixstop < iy; ixstop++) {
    CosHSum_All->data[ixstop] = 0.0;
  }

  emxInit_real_T(&SinH, 3);
  emxInit_real_T(&CosH, 3);
  emxInit_real_T(&R, 3);
  b_emxInit_real_T(&CosHSum, 2);
  b_emxInit_real_T(&CosHSumNorm, 2);
  b_emxInit_real_T(&tmpGamma, 2);
  b_emxInit_real_T(&r0, 2);
  c_emxInit_real_T(&x, 1);
  c_emxInit_real_T(&b_x, 1);
  b_emxInit_real_T(&y, 2);
  b_emxInit_real_T(&b, 2);
  b_emxInit_real_T(&r1, 2);
  emxInit_real_T(&r2, 3);
  emxInit_real_T(&r3, 3);
  b_emxInit_real_T(&r4, 2);
  emxInit_real_T(&b_SinH, 3);
  b_emxInit_real_T(&b_y, 2);
  c_emxInit_real_T(&b_R, 1);
  for (itmp = 0; itmp < 5; itmp++) {
    //  Initialize Mu and Kappa
    ixstop = Mu->size[0] * Mu->size[1];
    Mu->size[0] = (int)Num_of_clusters;
    Mu->size[1] = 4;
    emxEnsureCapacity((emxArray__common *)Mu, ixstop, (int)sizeof(double));
    iy = (int)Num_of_clusters << 2;
    for (ixstop = 0; ixstop < iy; ixstop++) {
      Mu->data[ixstop] = 0.0;
    }

    for (ix = 0; ix < (int)Num_of_clusters; ix++) {
      randn(Gamma);
      mtmp = norm(Gamma);
      for (ixstop = 0; ixstop < 4; ixstop++) {
        Gamma[ixstop] /= mtmp;
      }

      for (ixstop = 0; ixstop < 4; ixstop++) {
        Mu->data[ix + Mu->size[0] * ixstop] = Gamma[ixstop];
      }
    }

    ixstop = Kappa->size[0];
    Kappa->size[0] = (int)Num_of_clusters;
    emxEnsureCapacity((emxArray__common *)Kappa, ixstop, (int)sizeof(double));
    iy = (int)Num_of_clusters;
    for (ixstop = 0; ixstop < iy; ixstop++) {
      Kappa->data[ixstop] = 50.0;
    }

    mtmp = 1.0 / Num_of_clusters;
    ixstop = W->size[0];
    W->size[0] = (int)Num_of_clusters;
    emxEnsureCapacity((emxArray__common *)W, ixstop, (int)sizeof(double));
    iy = (int)Num_of_clusters;
    for (ixstop = 0; ixstop < iy; ixstop++) {
      W->data[ixstop] = mtmp;
    }

    memset(&L[0], 0, 30U * sizeof(double));
    ite = 0;
    exitg3 = false;
    while ((!exitg3) && (ite < 30)) {
      // %% E-step
      ixstop = SinH->size[0] * SinH->size[1] * SinH->size[2];
      SinH->size[0] = N;
      SinH->size[1] = No;
      SinH->size[2] = (int)Num_of_clusters;
      emxEnsureCapacity((emxArray__common *)SinH, ixstop, (int)sizeof(double));
      iy = N * No * (int)Num_of_clusters;
      for (ixstop = 0; ixstop < iy; ixstop++) {
        SinH->data[ixstop] = 0.0;
      }

      ixstop = CosH->size[0] * CosH->size[1] * CosH->size[2];
      CosH->size[0] = N;
      CosH->size[1] = No;
      CosH->size[2] = (int)Num_of_clusters;
      emxEnsureCapacity((emxArray__common *)CosH, ixstop, (int)sizeof(double));
      iy = N * No * (int)Num_of_clusters;
      for (ixstop = 0; ixstop < iy; ixstop++) {
        CosH->data[ixstop] = 0.0;
      }

      for (ix = 0; ix < (int)Num_of_clusters; ix++) {
        //  E-step
        for (j = 0; j < No; j++) {
          ixstop = tmpGamma->size[0] * tmpGamma->size[1];
          tmpGamma->size[0] = X->size[0];
          tmpGamma->size[1] = 4;
          emxEnsureCapacity((emxArray__common *)tmpGamma, ixstop, (int)sizeof
                            (double));
          mtmp = Kappa->data[ix];
          iy = X->size[0] * X->size[1];
          for (ixstop = 0; ixstop < iy; ixstop++) {
            tmpGamma->data[ixstop] = mtmp * X->data[ixstop];
          }

          for (ixstop = 0; ixstop < 4; ixstop++) {
            c_y[ixstop] = 0.0;
            for (n = 0; n < 4; n++) {
              mtmp = c_y[ixstop] + Pm->data[(ixstop + Pm->size[0] * n) +
                Pm->size[0] * Pm->size[1] * j] * Mu->data[ix + Mu->size[0] * n];
              c_y[ixstop] = mtmp;
            }
          }

          unnamed_idx_0 = (unsigned int)tmpGamma->size[0];
          ixstart = tmpGamma->size[0];
          ixstop = x->size[0];
          x->size[0] = (int)unnamed_idx_0;
          emxEnsureCapacity((emxArray__common *)x, ixstop, (int)sizeof(double));
          iy = (int)unnamed_idx_0;
          for (ixstop = 0; ixstop < iy; ixstop++) {
            x->data[ixstop] = 0.0;
          }

          if (tmpGamma->size[0] == 0) {
          } else {
            b_ix = 0;
            while ((ixstart > 0) && (b_ix <= 0)) {
              for (cindx = 1; cindx <= ixstart; cindx++) {
                x->data[cindx - 1] = 0.0;
              }

              b_ix = ixstart;
            }

            n = 0;
            b_ix = 0;
            while ((ixstart > 0) && (b_ix <= 0)) {
              vstride = 0;
              for (iy = n; iy + 1 <= n + 4; iy++) {
                if (c_y[iy] != 0.0) {
                  ixstop = vstride;
                  for (cindx = 0; cindx + 1 <= ixstart; cindx++) {
                    ixstop++;
                    x->data[cindx] += c_y[iy] * tmpGamma->data[ixstop - 1];
                  }
                }

                vstride += ixstart;
              }

              n += 4;
              b_ix = ixstart;
            }
          }

          mtmp = W->data[ix] * Cp(Kappa->data[ix]);
          b_cosh(x);
          iy = x->size[0];
          for (ixstop = 0; ixstop < iy; ixstop++) {
            CosH->data[(ixstop + CosH->size[0] * j) + CosH->size[0] * CosH->
              size[1] * ix] = mtmp * x->data[ixstop];
          }

          ixstop = tmpGamma->size[0] * tmpGamma->size[1];
          tmpGamma->size[0] = X->size[0];
          tmpGamma->size[1] = 4;
          emxEnsureCapacity((emxArray__common *)tmpGamma, ixstop, (int)sizeof
                            (double));
          mtmp = Kappa->data[ix];
          iy = X->size[0] * X->size[1];
          for (ixstop = 0; ixstop < iy; ixstop++) {
            tmpGamma->data[ixstop] = mtmp * X->data[ixstop];
          }

          for (ixstop = 0; ixstop < 4; ixstop++) {
            c_y[ixstop] = 0.0;
            for (n = 0; n < 4; n++) {
              mtmp = c_y[ixstop] + Pm->data[(ixstop + Pm->size[0] * n) +
                Pm->size[0] * Pm->size[1] * j] * Mu->data[ix + Mu->size[0] * n];
              c_y[ixstop] = mtmp;
            }
          }

          unnamed_idx_0 = (unsigned int)tmpGamma->size[0];
          ixstart = tmpGamma->size[0];
          ixstop = x->size[0];
          x->size[0] = (int)unnamed_idx_0;
          emxEnsureCapacity((emxArray__common *)x, ixstop, (int)sizeof(double));
          iy = (int)unnamed_idx_0;
          for (ixstop = 0; ixstop < iy; ixstop++) {
            x->data[ixstop] = 0.0;
          }

          if (tmpGamma->size[0] == 0) {
          } else {
            b_ix = 0;
            while ((ixstart > 0) && (b_ix <= 0)) {
              for (cindx = 1; cindx <= ixstart; cindx++) {
                x->data[cindx - 1] = 0.0;
              }

              b_ix = ixstart;
            }

            n = 0;
            b_ix = 0;
            while ((ixstart > 0) && (b_ix <= 0)) {
              vstride = 0;
              for (iy = n; iy + 1 <= n + 4; iy++) {
                if (c_y[iy] != 0.0) {
                  ixstop = vstride;
                  for (cindx = 0; cindx + 1 <= ixstart; cindx++) {
                    ixstop++;
                    x->data[cindx] += c_y[iy] * tmpGamma->data[ixstop - 1];
                  }
                }

                vstride += ixstart;
              }

              n += 4;
              b_ix = ixstart;
            }
          }

          mtmp = W->data[ix] * Cp(Kappa->data[ix]);
          b_sinh(x);
          iy = x->size[0];
          for (ixstop = 0; ixstop < iy; ixstop++) {
            SinH->data[(ixstop + SinH->size[0] * j) + SinH->size[0] * SinH->
              size[1] * ix] = mtmp * x->data[ixstop];
          }
        }
      }

      sum(CosH, r1);
      b_sum(r1, x);
      dv0[0] = 1.0;
      dv0[1] = No;
      dv0[2] = Num_of_clusters;
      repmat(x, dv0, r2);
      rdivide(SinH, r2, R);

      // %% M-step
      //  estimate W
      c_sum(CosH, r3);
      squeeze(r3, CosHSum);
      b_sum(CosHSum, x);
      dv1[0] = 1.0;
      dv1[1] = Num_of_clusters;
      b_repmat(x, dv1, r1);
      b_rdivide(CosHSum, r1, CosHSumNorm);
      d_sum(CosHSumNorm, r4);
      ixstop = W->size[0];
      W->size[0] = r4->size[1];
      emxEnsureCapacity((emxArray__common *)W, ixstop, (int)sizeof(double));
      iy = r4->size[1];
      for (ixstop = 0; ixstop < iy; ixstop++) {
        W->data[ixstop] = r4->data[r4->size[0] * ixstop];
      }

      b_W[0] = W->size[0];
      c_W = *W;
      c_W.size = (int *)&b_W;
      c_W.numDimensions = 1;
      mtmp = e_sum(&c_W);
      ixstop = W->size[0];
      emxEnsureCapacity((emxArray__common *)W, ixstop, (int)sizeof(double));
      iy = W->size[0];
      for (ixstop = 0; ixstop < iy; ixstop++) {
        W->data[ixstop] /= mtmp;
      }

      for (ix = 0; ix < (int)Num_of_clusters; ix++) {
        //  estimate Mu
        ixstop = tmpGamma->size[0] * tmpGamma->size[1];
        tmpGamma->size[0] = No;
        tmpGamma->size[1] = 4;
        emxEnsureCapacity((emxArray__common *)tmpGamma, ixstop, (int)sizeof
                          (double));
        iy = No << 2;
        for (ixstop = 0; ixstop < iy; ixstop++) {
          tmpGamma->data[ixstop] = 0.0;
        }

        for (j = 0; j < No; j++) {
          for (ixstop = 0; ixstop < 4; ixstop++) {
            for (n = 0; n < 4; n++) {
              a[n + (ixstop << 2)] = Pm->data[(ixstop + Pm->size[0] * n) +
                Pm->size[0] * Pm->size[1] * j];
            }
          }

          ixstop = b->size[0] * b->size[1];
          b->size[0] = 4;
          b->size[1] = X->size[0];
          emxEnsureCapacity((emxArray__common *)b, ixstop, (int)sizeof(double));
          iy = X->size[0];
          for (ixstop = 0; ixstop < iy; ixstop++) {
            for (n = 0; n < 4; n++) {
              b->data[n + b->size[0] * ixstop] = X->data[ixstop + X->size[0] * n];
            }
          }

          unnamed_idx_0 = (unsigned int)b->size[1];
          ixstop = y->size[0] * y->size[1];
          y->size[0] = 4;
          emxEnsureCapacity((emxArray__common *)y, ixstop, (int)sizeof(double));
          ixstop = y->size[0] * y->size[1];
          y->size[1] = (int)unnamed_idx_0;
          emxEnsureCapacity((emxArray__common *)y, ixstop, (int)sizeof(double));
          iy = (int)unnamed_idx_0 << 2;
          for (ixstop = 0; ixstop < iy; ixstop++) {
            y->data[ixstop] = 0.0;
          }

          if (b->size[1] == 0) {
          } else {
            ixstart = (b->size[1] - 1) << 2;
            for (b_ix = 0; b_ix <= ixstart; b_ix += 4) {
              for (cindx = b_ix; cindx + 1 <= b_ix + 4; cindx++) {
                y->data[cindx] = 0.0;
              }
            }

            n = 0;
            for (b_ix = 0; b_ix <= ixstart; b_ix += 4) {
              vstride = 0;
              for (iy = n; iy + 1 <= n + 4; iy++) {
                if (b->data[iy] != 0.0) {
                  ixstop = vstride;
                  for (cindx = b_ix; cindx + 1 <= b_ix + 4; cindx++) {
                    ixstop++;
                    y->data[cindx] += b->data[iy] * a[ixstop - 1];
                  }
                }

                vstride += 4;
              }

              n += 4;
            }
          }

          iy = R->size[0];
          ixstop = b_R->size[0];
          b_R->size[0] = iy;
          emxEnsureCapacity((emxArray__common *)b_R, ixstop, (int)sizeof(double));
          for (ixstop = 0; ixstop < iy; ixstop++) {
            b_R->data[ixstop] = R->data[(ixstop + R->size[0] * j) + R->size[0] *
              R->size[1] * ix];
          }

          c_repmat(b_R, r0);
          ixstop = b_y->size[0] * b_y->size[1];
          b_y->size[0] = y->size[1];
          b_y->size[1] = 4;
          emxEnsureCapacity((emxArray__common *)b_y, ixstop, (int)sizeof(double));
          for (ixstop = 0; ixstop < 4; ixstop++) {
            iy = y->size[1];
            for (n = 0; n < iy; n++) {
              b_y->data[n + b_y->size[0] * ixstop] = y->data[ixstop + y->size[0]
                * n] * r0->data[n + r0->size[0] * ixstop];
            }
          }

          f_sum(b_y, dv2);
          for (ixstop = 0; ixstop < 4; ixstop++) {
            tmpGamma->data[j + tmpGamma->size[0] * ixstop] = dv2[ixstop];
          }
        }

        g_sum(tmpGamma, Gamma);
        mtmp = norm(Gamma);
        for (ixstop = 0; ixstop < 4; ixstop++) {
          Mu->data[ix + Mu->size[0] * ixstop] = Gamma[ixstop] / mtmp;
        }

        //  estimate Kappa
        Kappa->data[ix] = invAp(norm(Gamma) / (W->data[ix] * (double)N));
      }

      ixstop = SinH->size[0] * SinH->size[1] * SinH->size[2];
      SinH->size[0] = N;
      SinH->size[1] = No;
      SinH->size[2] = (int)Num_of_clusters;
      emxEnsureCapacity((emxArray__common *)SinH, ixstop, (int)sizeof(double));
      iy = N * No * (int)Num_of_clusters;
      for (ixstop = 0; ixstop < iy; ixstop++) {
        SinH->data[ixstop] = 0.0;
      }

      for (ix = 0; ix < (int)Num_of_clusters; ix++) {
        for (j = 0; j < No; j++) {
          ixstop = tmpGamma->size[0] * tmpGamma->size[1];
          tmpGamma->size[0] = X->size[0];
          tmpGamma->size[1] = 4;
          emxEnsureCapacity((emxArray__common *)tmpGamma, ixstop, (int)sizeof
                            (double));
          mtmp = Kappa->data[ix];
          iy = X->size[0] * X->size[1];
          for (ixstop = 0; ixstop < iy; ixstop++) {
            tmpGamma->data[ixstop] = mtmp * X->data[ixstop];
          }

          for (ixstop = 0; ixstop < 4; ixstop++) {
            c_y[ixstop] = 0.0;
            for (n = 0; n < 4; n++) {
              mtmp = c_y[ixstop] + Pm->data[(ixstop + Pm->size[0] * n) +
                Pm->size[0] * Pm->size[1] * j] * Mu->data[ix + Mu->size[0] * n];
              c_y[ixstop] = mtmp;
            }
          }

          unnamed_idx_0 = (unsigned int)tmpGamma->size[0];
          ixstart = tmpGamma->size[0];
          ixstop = x->size[0];
          x->size[0] = (int)unnamed_idx_0;
          emxEnsureCapacity((emxArray__common *)x, ixstop, (int)sizeof(double));
          iy = (int)unnamed_idx_0;
          for (ixstop = 0; ixstop < iy; ixstop++) {
            x->data[ixstop] = 0.0;
          }

          if (tmpGamma->size[0] == 0) {
          } else {
            b_ix = 0;
            while ((ixstart > 0) && (b_ix <= 0)) {
              for (cindx = 1; cindx <= ixstart; cindx++) {
                x->data[cindx - 1] = 0.0;
              }

              b_ix = ixstart;
            }

            n = 0;
            b_ix = 0;
            while ((ixstart > 0) && (b_ix <= 0)) {
              vstride = 0;
              for (iy = n; iy + 1 <= n + 4; iy++) {
                if (c_y[iy] != 0.0) {
                  ixstop = vstride;
                  for (cindx = 0; cindx + 1 <= ixstart; cindx++) {
                    ixstop++;
                    x->data[cindx] += c_y[iy] * tmpGamma->data[ixstop - 1];
                  }
                }

                vstride += ixstart;
              }

              n += 4;
              b_ix = ixstart;
            }
          }

          ixstop = b_x->size[0];
          b_x->size[0] = x->size[0];
          emxEnsureCapacity((emxArray__common *)b_x, ixstop, (int)sizeof(double));
          iy = x->size[0];
          for (ixstop = 0; ixstop < iy; ixstop++) {
            b_x->data[ixstop] = x->data[ixstop];
          }

          for (vstride = 0; vstride < x->size[0]; vstride++) {
            b_x->data[vstride] = cosh(b_x->data[vstride]);
          }

          mtmp = 2.0 * W->data[ix] * Cp(Kappa->data[ix]);
          iy = b_x->size[0];
          for (ixstop = 0; ixstop < iy; ixstop++) {
            SinH->data[(ixstop + SinH->size[0] * j) + SinH->size[0] * SinH->
              size[1] * ix] = mtmp * b_x->data[ixstop];
          }
        }
      }

      ixstop = b_SinH->size[0] * b_SinH->size[1] * b_SinH->size[2];
      b_SinH->size[0] = SinH->size[0];
      b_SinH->size[1] = SinH->size[1];
      b_SinH->size[2] = SinH->size[2];
      emxEnsureCapacity((emxArray__common *)b_SinH, ixstop, (int)sizeof(double));
      iy = SinH->size[0] * SinH->size[1] * SinH->size[2];
      for (ixstop = 0; ixstop < iy; ixstop++) {
        b_SinH->data[ixstop] = SinH->data[ixstop] + 2.2204460492503131E-16;
      }

      sum(b_SinH, r1);
      b_sum(r1, b_x);
      ixstop = x->size[0];
      x->size[0] = b_x->size[0];
      emxEnsureCapacity((emxArray__common *)x, ixstop, (int)sizeof(double));
      iy = b_x->size[0];
      for (ixstop = 0; ixstop < iy; ixstop++) {
        x->data[ixstop] = b_x->data[ixstop];
      }

      for (vstride = 0; vstride < b_x->size[0]; vstride++) {
        x->data[vstride] = log(x->data[vstride]);
      }

      L[ite] = e_sum(x);

      //  update the containers
      for (ixstop = 0; ixstop < 4; ixstop++) {
        iy = Mu->size[0];
        for (n = 0; n < iy; n++) {
          Mu_All->data[(n + Mu_All->size[0] * ixstop) + Mu_All->size[0] *
            Mu_All->size[1] * itmp] = Mu->data[n + Mu->size[0] * ixstop];
        }
      }

      ixstart = Kappa->size[0];
      for (ixstop = 0; ixstop < ixstart; ixstop++) {
        Kappa_All->data[ixstop + Kappa_All->size[0] * itmp] = Kappa->data[ixstop];
      }

      ixstart = W->size[0];
      for (ixstop = 0; ixstop < ixstart; ixstop++) {
        W_All->data[ixstop + W_All->size[0] * itmp] = W->data[ixstop];
      }

      L_All[itmp] = L[ite];
      iy = CosHSumNorm->size[1];
      for (ixstop = 0; ixstop < iy; ixstop++) {
        b_ix = CosHSumNorm->size[0];
        for (n = 0; n < b_ix; n++) {
          CosHSum_All->data[(n + CosHSum_All->size[0] * ixstop) +
            CosHSum_All->size[0] * CosHSum_All->size[1] * itmp] =
            CosHSumNorm->data[n + CosHSumNorm->size[0] * ixstop];
        }
      }

      //  Check stopping criteria
      if ((1 + ite >= 2) && (fabs(L[ite] - L[ite - 1]) < 0.05)) {
        exitg3 = true;
      } else {
        ite++;
      }
    }
  }

  emxFree_real_T(&b_R);
  emxFree_real_T(&b_y);
  emxFree_real_T(&b_SinH);
  emxFree_real_T(&r4);
  emxFree_real_T(&r3);
  emxFree_real_T(&r2);
  emxFree_real_T(&b);
  emxFree_real_T(&y);
  emxFree_real_T(&b_x);
  emxFree_real_T(&x);
  emxFree_real_T(&r0);
  emxFree_real_T(&tmpGamma);
  emxFree_real_T(&CosHSumNorm);
  emxFree_real_T(&R);
  emxFree_real_T(&CosH);
  emxFree_real_T(&SinH);
  ixstart = 1;
  mtmp = L_All[0];
  itmp = 0;
  if (rtIsNaN(L_All[0])) {
    b_ix = 1;
    exitg2 = false;
    while ((!exitg2) && (b_ix + 1 < 6)) {
      ixstart = b_ix + 1;
      if (!rtIsNaN(L_All[b_ix])) {
        mtmp = L_All[b_ix];
        itmp = b_ix;
        exitg2 = true;
      } else {
        b_ix++;
      }
    }
  }

  if (ixstart < 5) {
    while (ixstart + 1 < 6) {
      if (L_All[ixstart] > mtmp) {
        mtmp = L_All[ixstart];
        itmp = ixstart;
      }

      ixstart++;
    }
  }

  iy = Mu_All->size[0];
  ixstop = Mu->size[0] * Mu->size[1];
  Mu->size[0] = iy;
  Mu->size[1] = 4;
  emxEnsureCapacity((emxArray__common *)Mu, ixstop, (int)sizeof(double));
  for (ixstop = 0; ixstop < 4; ixstop++) {
    for (n = 0; n < iy; n++) {
      Mu->data[n + Mu->size[0] * ixstop] = Mu_All->data[(n + Mu_All->size[0] *
        ixstop) + Mu_All->size[0] * Mu_All->size[1] * itmp];
    }
  }

  emxFree_real_T(&Mu_All);
  iy = Kappa_All->size[0];
  ixstop = Kappa->size[0];
  Kappa->size[0] = iy;
  emxEnsureCapacity((emxArray__common *)Kappa, ixstop, (int)sizeof(double));
  for (ixstop = 0; ixstop < iy; ixstop++) {
    Kappa->data[ixstop] = Kappa_All->data[ixstop + Kappa_All->size[0] * itmp];
  }

  emxFree_real_T(&Kappa_All);
  iy = W_All->size[0];
  ixstop = W->size[0];
  W->size[0] = iy;
  emxEnsureCapacity((emxArray__common *)W, ixstop, (int)sizeof(double));
  for (ixstop = 0; ixstop < iy; ixstop++) {
    W->data[ixstop] = W_All->data[ixstop + W_All->size[0] * itmp];
  }

  emxFree_real_T(&W_All);
  *logL = L_All[itmp];
  if (Num_of_clusters > 1.0) {
    ixstop = CosHSum_All->size[0];
    n = CosHSum_All->size[1];
    ixstart = CosHSum->size[0] * CosHSum->size[1];
    CosHSum->size[0] = ixstop;
    CosHSum->size[1] = n;
    emxEnsureCapacity((emxArray__common *)CosHSum, ixstart, (int)sizeof(double));
    ixstop = CosHSum_All->size[0];
    n = CosHSum_All->size[1];
    ixstop *= n;
    for (vstride = 0; vstride + 1 <= ixstop; vstride++) {
      iy = CosHSum_All->size[0];
      b_ix = CosHSum_All->size[1];
      n = r1->size[0] * r1->size[1];
      r1->size[0] = iy;
      r1->size[1] = b_ix;
      emxEnsureCapacity((emxArray__common *)r1, n, (int)sizeof(double));
      for (n = 0; n < b_ix; n++) {
        for (ixstart = 0; ixstart < iy; ixstart++) {
          r1->data[ixstart + r1->size[0] * n] = CosHSum_All->data[(ixstart +
            CosHSum_All->size[0] * n) + CosHSum_All->size[0] * CosHSum_All->
            size[1] * itmp];
        }
      }

      n = r1->size[0];
      CosHSum->data[vstride] = r1->data[vstride % r1->size[0] + r1->size[0] *
        div_nzp_s32_floor(vstride, n)];
    }

    for (ixstop = 0; ixstop < 2; ixstop++) {
      outsz[ixstop] = (unsigned int)CosHSum->size[ixstop];
    }

    emxInit_int32_T(&iindx, 1);
    ixstop = iindx->size[0];
    iindx->size[0] = (int)outsz[0];
    emxEnsureCapacity((emxArray__common *)iindx, ixstop, (int)sizeof(int));
    iy = (int)outsz[0];
    for (ixstop = 0; ixstop < iy; ixstop++) {
      iindx->data[ixstop] = 1;
    }

    n = CosHSum->size[1];
    vstride = CosHSum->size[0];
    b_ix = 0;
    iy = -1;
    for (j = 1; j <= vstride; j++) {
      b_ix++;
      ixstart = b_ix;
      ixstop = b_ix + (n - 1) * vstride;
      mtmp = CosHSum->data[b_ix - 1];
      itmp = 1;
      if (n > 1) {
        cindx = 1;
        if (rtIsNaN(CosHSum->data[b_ix - 1])) {
          ix = b_ix + vstride;
          exitg1 = false;
          while ((!exitg1) && ((vstride > 0) && (ix <= ixstop))) {
            cindx++;
            ixstart = ix;
            if (!rtIsNaN(CosHSum->data[ix - 1])) {
              mtmp = CosHSum->data[ix - 1];
              itmp = cindx;
              exitg1 = true;
            } else {
              ix += vstride;
            }
          }
        }

        if (ixstart < ixstop) {
          ix = ixstart + vstride;
          while ((vstride > 0) && (ix <= ixstop)) {
            cindx++;
            if (CosHSum->data[ix - 1] > mtmp) {
              mtmp = CosHSum->data[ix - 1];
              itmp = cindx;
            }

            ix += vstride;
          }
        }
      }

      iy++;
      iindx->data[iy] = itmp;
    }

    ixstop = CIdx->size[0];
    CIdx->size[0] = iindx->size[0];
    emxEnsureCapacity((emxArray__common *)CIdx, ixstop, (int)sizeof(double));
    iy = iindx->size[0];
    for (ixstop = 0; ixstop < iy; ixstop++) {
      CIdx->data[ixstop] = iindx->data[ixstop];
    }

    emxFree_int32_T(&iindx);
  } else {
    ixstop = CIdx->size[0];
    CIdx->size[0] = X->size[0];
    emxEnsureCapacity((emxArray__common *)CIdx, ixstop, (int)sizeof(double));
    iy = X->size[0];
    for (ixstop = 0; ixstop < iy; ixstop++) {
      CIdx->data[ixstop] = 1.0;
    }
  }

  emxFree_real_T(&r1);
  emxFree_real_T(&CosHSum);
  emxFree_real_T(&CosHSum_All);
}

//
// File trailer for EMmVMF.cpp
//
// [EOF]
//
