//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: EMmVMF_data.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:04:28
//

// Include Files
#include "rt_nonfinite.h"
#include "EMmVMF.h"
#include "EMmVMF_data.h"

// Variable Definitions
unsigned int state[625];

//
// File trailer for EMmVMF_data.cpp
//
// [EOF]
//
