//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: squeeze.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:04:28
//

// Include Files
#include "rt_nonfinite.h"
#include "EMmVMF.h"
#include "squeeze.h"
#include "EMmVMF_emxutil.h"

// Function Definitions

//
// Arguments    : const emxArray_real_T *a
//                emxArray_real_T *b
// Return Type  : void
//
void squeeze(const emxArray_real_T *a, emxArray_real_T *b)
{
  int k;
  int i1;
  int sqsz[3];
  k = 3;
  while ((k > 2) && (a->size[2] == 1)) {
    k = 2;
  }

  if (k <= 2) {
    k = a->size[0];
    i1 = b->size[0] * b->size[1];
    b->size[0] = k;
    b->size[1] = 1;
    emxEnsureCapacity((emxArray__common *)b, i1, (int)sizeof(double));
    i1 = a->size[0] * a->size[2];
    for (k = 0; k + 1 <= i1; k++) {
      b->data[k] = a->data[k];
    }
  } else {
    for (i1 = 0; i1 < 3; i1++) {
      sqsz[i1] = 1;
    }

    k = 0;
    if (a->size[0] != 1) {
      sqsz[0] = a->size[0];
      k = 1;
    }

    if (a->size[2] != 1) {
      sqsz[k] = a->size[2];
    }

    i1 = b->size[0] * b->size[1];
    b->size[0] = sqsz[0];
    b->size[1] = sqsz[1];
    emxEnsureCapacity((emxArray__common *)b, i1, (int)sizeof(double));
    i1 = a->size[0] * a->size[2];
    for (k = 0; k + 1 <= i1; k++) {
      b->data[k] = a->data[k];
    }
  }
}

//
// File trailer for squeeze.cpp
//
// [EOF]
//
