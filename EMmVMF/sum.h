//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: sum.h
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:04:28
//
#ifndef __SUM_H__
#define __SUM_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "EMmVMF_types.h"

// Function Declarations
extern void b_sum(const emxArray_real_T *x, emxArray_real_T *y);
extern void c_sum(const emxArray_real_T *x, emxArray_real_T *y);
extern void d_sum(const emxArray_real_T *x, emxArray_real_T *y);
extern double e_sum(const emxArray_real_T *x);
extern void f_sum(const emxArray_real_T *x, double y[4]);
extern void g_sum(const emxArray_real_T *x, double y[4]);
extern void sum(const emxArray_real_T *x, emxArray_real_T *y);

#endif

//
// File trailer for sum.h
//
// [EOF]
//
