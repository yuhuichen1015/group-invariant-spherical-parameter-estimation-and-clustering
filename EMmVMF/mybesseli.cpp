//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: mybesseli.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 28-Jul-2015 16:26:21
//

// Include Files
#include "rt_nonfinite.h"
#include "EMmVMF.h"
#include "mybesseli.h"

#include <boost/math/special_functions/bessel.hpp> // for cyl_bessel_i
// Function Definitions

//
// Arguments    : double x
// Return Type  : double
//
double mybesseli(double x)
{
  // %% Empty Besseli function should be replaced by originated C++ coes.
  //      value = besseli(p,x);
  return boost::math::cyl_bessel_i(1.0, x);
  // return 0.1 * x;
}

//
// File trailer for mybesseli.cpp
//
// [EOF]
//
