//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: EMmVMF_rtwutil.h
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 28-Jul-2015 23:49:49
//
#ifndef __EMMVMF_RTWUTIL_H__
#define __EMMVMF_RTWUTIL_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "EMmVMF_types.h"

// Function Declarations
extern double rt_powd_snf(double u0, double u1);

#endif

//
// File trailer for EMmVMF_rtwutil.h
//
// [EOF]
//
