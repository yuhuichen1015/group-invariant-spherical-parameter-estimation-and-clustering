clear;
SymOperatorsSrp;

% Generate the quaternions
QuatRef = [-0.0058    0.7298   -0.6206    0.2866];

Quat = zeros(size(Pm,3), 4);
for p=1:size(Pm,3)
    Quat(p,:) = (Pm(:,:,p)*QuatRef(:))';
end
Quat

% Perform the transformation toward a reference quaterion.
Qtrans = TransformToRef(QuatRef, Quat, Pm);
Qtrans  % Each row should be the same;

% Perform the transformation toward a reference quaterion.
Qtrans = TransformToRef_mex(QuatRef, Quat, Pm);
Qtrans  % Each row should be the same;