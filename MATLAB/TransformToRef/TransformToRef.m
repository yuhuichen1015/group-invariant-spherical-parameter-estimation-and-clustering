function [QuatT] = TransformToRef(QuatRef, Quat, Pm)
% This function convert the quaternions towards a reference direction (in
% quaternion) with one of the symmetry operator defined in Pm.
%
% Usage:
%   [QuatFZ] = Quat2CubicFZ(Quat)
%
% Inputs:
%   QuatRef: A 1 x 4 vector which is the quaternion of the reference
%           direction.
%
%   Quat : A N x 4 matrix which contains the N quaternions that need to be
%           converted.
%
%   Pm : A 4 x 4 x M matrx where each of the 4 x 4 submatrix is the
%       symmetry operator in rotation orthnormal matrix form.
%
% Outputs:
%   QuatT : A N x 4 matrix which contains the N quaternions that are
%       pointing toward QuatRef.
%
% Function is written by Yu-Hui Chen, University of Michigan

if(size(QuatRef,2)~=4 || size(Quat,2)~=4 || size(Pm,1)~=4 || size(Pm,2)~=4)
    error('The inputs should be in quaternion space. (4x1 for QuatRef, 4xN matrix for Quat, and 4x4xM for Pm');
end
QuatT = zeros(size(Quat));
N = size(Quat,1);
M = size(Pm,3);
% Transforming all angles to the be closest to the first one
for n=1:N
    sym = zeros(M,1);
    for j=1:M
        sym(j) = (QuatRef*(Pm(:,:,j)'*Quat(n,:)'));
    end
    [cs, dd] = max(abs(sym));
    if(sym(dd)<0)
        QuatT(n,:) = -(Pm(:,:,dd)'*Quat(n,:)')';
    else
        QuatT(n,:) = (Pm(:,:,dd)'*Quat(n,:)')';
    end
end

end