clear;

%%% Load the operators
SymOperatorsSrp;

%% Test for 1 cluster parameter estimation.
% Load the testing data
load('VMFSamples_1C.mat');

%%% Testing WastonEM
global xAp;
global yAp;
xAp=0.001:0.1:700;
yAp = Ap(xAp, 4);
[Mu, Kappa, W, logL] = EMmVMF(X, Pm);
[cs, Mu_est] = SymDistance(Mu_truth, Mu, Pm);
cs
Kappa
W
logL

[Mu, Kappa, W, logL] = EMmVMF_mex(X, Pm, 1);
[cs, Mu_est] = SymDistance(Mu_truth, Mu, Pm);
cs
Kappa
W
logL

%% Test for 2 clusters parameter estimation.
% Load the testing data
load('VMFSamples_2C.mat');

%%% Testing WastonEM
[Mu, Kappa, W, logL, CIdx] = EMmVMF(X, Pm, 2);
if(W(1)<0.5)
    [cs1, Mu_est] = SymDistance(Mu1_truth, Mu(1,:), Pm);
    [cs2, Mu_est] = SymDistance(Mu2_truth, Mu(2,:), Pm);
    cs1
    cs2
else
    [cs1, Mu_est] = SymDistance(Mu2_truth, Mu(1,:), Pm);
    [cs2, Mu_est] = SymDistance(Mu1_truth, Mu(2,:), Pm);
    cs1
    cs2
end
Kappa
W
logL
sum(CIdx(:)==1)/numel(CIdx)

%%% Testing WastonEM
[Mu, Kappa, W, logL, CIdx] = EMmVMF_mex(X, Pm, 2);
if(W(1)<0.5)
    [cs1, Mu_est] = SymDistance(Mu1_truth, Mu(1,:), Pm);
    [cs2, Mu_est] = SymDistance(Mu2_truth, Mu(2,:), Pm);
    cs1
    cs2
else
    [cs1, Mu_est] = SymDistance(Mu2_truth, Mu(1,:), Pm);
    [cs2, Mu_est] = SymDistance(Mu1_truth, Mu(2,:), Pm);
    cs1
    cs2
end
Kappa
W
logL
sum(CIdx(:)==1)/numel(CIdx)
