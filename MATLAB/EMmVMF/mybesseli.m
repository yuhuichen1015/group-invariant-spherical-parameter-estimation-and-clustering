function [value] = mybesseli(p, x)
    coder.inline('never');
%     coder.extrinsic('besseli');
    %%% Empty Besseli function should be replaced by originated C++ coes.
%     value = besseli(p,x);
    value = 0.1*x;
end