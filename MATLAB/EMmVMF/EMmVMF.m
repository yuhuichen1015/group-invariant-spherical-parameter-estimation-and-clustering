function [Mu, Kappa, W, logL, CIdx]=EMmVMF(X, Pm, Num_of_clusters)
% This is the EM algorithm for estimating the parameters of group-invariant
% von Mises-Fisher distribution. It also supports the clustering of
% group-invariant samples. Details of the algorithm can be found in the
% following paper:
%   Yu-Hui Chen, Dennis Wei, Gregory Newstadt, Marc DeGraef, 
%   Jeffrey Simmons, Alfred Hero "Statistical Estimation and Clustering of 
%   Group-invariant Orientation Parameters" *International Conference on 
%   Information Fusion, 2015
% 
% Usage:
%  [Mu, Kappa, W, logL, CIdx]=EMmVMF(X, Pm, Num_of_clusters(optional))
% 
% Inputs:
%   X : A N x 4 matrix which contains the N quaternion samples which need to
%       be clustered or estimated its mean and concentration.
%
%   Pm : A 4 x 4 x M matrx where each of the 4 x 4 submatrix is the
%       symmetry operator in rotation orthnormal matrix form.
%
%   Num_of_clusters : An integer which specify the number of clusters in
%       the samples.
%   
%  
% Outputs:
%   Mu : A Num_of_clusters x 4 matrix which contains the estimated means of
%       each cluster in quaternion.
%
%   Kappa : A Num_of_clusters x 1 matrix which contains the estimated kappa
%       of each cluster.
%
%   W : A Num_of_clusters x 1 matrix which contains the mixing weight of
%       the clusters.
%
%   logL : The log-likelihood for the samples of the estimated model.
%
%   CIdx : The N x 1 matrix which indicates the cluster of each sample it
%       belongs to.
%  
% Function is written by Yu-Hui Chen, University of Michigan

%%% Duplicate the Euler Angles
No = size(Pm,3);
N = size(X,1);
p = size(X,2);

% Precompute the xAp, yAp for invAp_fast
global xAp;
global yAp;

% Create container for estimated parameters
if(nargin<3)
    Num_of_clusters=1;
end
Num_of_init=5;
Mu_All = zeros(Num_of_clusters, p, Num_of_init);
Kappa_All = zeros(Num_of_clusters, Num_of_init);
W_All = zeros(Num_of_clusters, Num_of_init);
L_All = zeros(Num_of_init,1);
CosHSum_All = zeros(N, Num_of_clusters, Num_of_init);
for init=1:Num_of_init
    % Initialize Mu and Kappa
    Mu = zeros(Num_of_clusters, p);
    for clu = 1:Num_of_clusters
        mu = randn(1,p);
        mu = mu/norm(mu,2);
        Mu(clu,:) = mu(:)';
    end
    Kappa = 50*ones(Num_of_clusters,1);
    W = (1/Num_of_clusters)*ones(Num_of_clusters,1);
    
    Num_of_iteration=30;
    L=zeros(Num_of_iteration,1);
    for ite=1:Num_of_iteration
        %%% E-step
        SinH = zeros(N, No, Num_of_clusters);
        CosH = zeros(N, No, Num_of_clusters);
        for clu = 1:Num_of_clusters
            % E-step
            for j=1:No
                CosH(:,j,clu) = W(clu)*Cp(Kappa(clu),p)*cosh(Kappa(clu)*X*(Pm(:,:,j)*Mu(clu,:)'));
                SinH(:,j,clu) = W(clu)*Cp(Kappa(clu),p)*sinh(Kappa(clu)*X*(Pm(:,:,j)*Mu(clu,:)'));
            end
        end
        R = SinH ./ repmat(sum(sum(CosH,3),2), [1 No Num_of_clusters]);
        
        %%% M-step
        % estimate W
        CosHSum = squeeze(sum(CosH,2));
        CosHSumNorm = CosHSum ./ repmat(sum(CosHSum,2), [1, Num_of_clusters]);
        W = sum(CosHSumNorm,1)';
        W = W / sum(W(:));
        for clu = 1:Num_of_clusters
            % estimate Mu
            tmpGamma = zeros(No, p);
            for j=1:No
                tmpGamma(j,:) = sum((Pm(:,:,j)'*X')'.*repmat(R(:,j,clu), [1 4]));
            end
            Gamma = sum(tmpGamma,1);
            Mu(clu,:) = Gamma / norm(Gamma,2);
            % estimate Kappa
            Kappa(clu) = invAp(norm(Gamma,2)/(W(clu)*N), p, xAp, yAp);
        end
        
        PhiT = zeros(N,No,Num_of_clusters);
        for clu = 1:Num_of_clusters
            for j=1:No
                PhiT(:,j,clu) = 2*W(clu)*Cp(Kappa(clu),p)*cosh(Kappa(clu)*X*(Pm(:,:,j)*Mu(clu,:)'));
            end
        end
        L(ite) = sum(log(sum(sum(PhiT+eps,3),2)));
        
        
        % update the containers
        Mu_All(:,:,init) = Mu;
        Kappa_All(:,init) = Kappa(:);
        W_All(:,init) = W(:);
        L_All(init) = L(ite);
        CosHSum_All(:,:,init) = CosHSumNorm;
        
        % Check stopping criteria
        if(ite>=2)
            if(abs(L(ite)-L(ite-1))<0.05)
                break;
            end
        end
    end
end
[yy, dd] = max(L_All);
Mu=Mu_All(:,:,dd);
Kappa=Kappa_All(:,dd);
W = W_All(:,dd);
logL = L_All(dd);
if(Num_of_clusters>1)
    [yy,CIdx] = max(squeeze(CosHSum_All(:,:,dd)),[],2);
else
    CIdx = ones(N,1);
end

end