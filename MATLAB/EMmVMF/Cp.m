function [y] = Cp(kappa, p)
%   This function is used for calculate the density of Von-Mises Fisher
%   distribution.  
% 
% inputs,
%   kappa : It is the kappa parameter of the Von-Mises Fisher distribution.
%
%   p : It is the dimension of Von-Mises Fisher distribution which is an
%       integer.
%
% outputs,
%   y : It is the output value of this function.
%
% Function is written by Yu-Hui Chen, University of Michigan
     y = (2*pi)^(-p/2) * (kappa.^(p/2-1) ./ mybesseli(p/2-1, kappa));
end