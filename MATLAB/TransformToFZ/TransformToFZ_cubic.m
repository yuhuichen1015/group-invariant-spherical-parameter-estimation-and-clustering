function [QuatFZ] = TransformToFZ_cubic(Quat)

% This function convert the quaternions into pre-defined fundamental zone
% for cubic symmetry.
% 
% Usage:
%  [QuatFZ] = Quat2CubicFZ(Quat)
% 
% Inputs:
%   Quat : A N x 4 matrix which contains the N quaternions that need to be
%           converted into fundamental zone. 
%   
%  
% Outputs:
%   QuatFZ : A N x 4 matrix which contains the N quaternions that are in the 
%           fundamental zone. 
%  
% Function is written by Yu-Hui Chen, University of Michigan

global Pm;

%%% Start to convert the quaternions into fundamental zone
Sz = size(Quat);
if(Sz(2)~=4)
    error('wrong inputs');
end
QuatFZ = zeros(Sz);
for s=1:Sz(1) % Loop through each input quaternion
    repRod = zeros(3,24);
    for j=1:24 % replicate the quaternion by the 24 operators
        q = Pm(:,:,j)*Quat(s,:)';
        repRod(:,j) = [q(1)/q(4); q(2)/q(4); q(3)/q(4)];
    end
    for idx=1:24 % Loop through each replicates to check which one is in FZ
        r1=repRod(1,idx);
        r2=repRod(2,idx);
        r3=repRod(3,idx);
        % The inequalities which defined the FZ
        if(abs(r1)<=sqrt(2)-1 && abs(r2)<=sqrt(2)-1 && abs(r3)<=sqrt(2)-1)
            if(abs(r1+r2+r3)<=1 && abs(r1-r2+r3)<=1 && abs(r1+r2-r3)<=1 && abs(-r1+r2+r3)<=1)
                break
            end
        end
    end
    % Convert Rodrigues vec back to quaternion
    R = repRod(:,idx);
    q=zeros(4,1);
    normR2 = norm(R,2)^2;
    q(4) = 1/sqrt(1+normR2);
    q(1) = R(1)/sqrt(1+normR2);
    q(2) = R(2)/sqrt(1+normR2);
    q(3) = R(3)/sqrt(1+normR2);
    QuatFZ(s,:) = q;
end

end