clear;
SymOperatorsSrp;
global Pm;

% Generate the quaternions
QuatRef = [-0.0058    0.7298   -0.6206    0.2866];

Quat = zeros(size(Pm,3), 4);
for p=1:size(Pm,3)
    Quat(p,:) = (Pm(:,:,p)*QuatRef(:))';
end
Quat

% Perform the transformation toward a reference quaterion.
Qtrans = TransformToFZ_cubic(Quat);
Qtrans  % Each row should be the same;


QtransMex = TransformToFZ_cubic_mex(Quat);
QtransMex