clear;

%%% Load the operators
SymOperatorsSrp;

global xDp;
global yDp;
xDp=-35:1:490;
yDp = Dp(xDp, 4);

%% Test for 1 cluster parameter estimation.
% Load the testing data
load('WatsonSamples_1C.mat');
%%% Testing WastonEM
[Mu, Kappa] = EMmWatson(X, Pm);
[cs, Mu_est] = SymDistance(Mu_truth, Mu, Pm);
cs
Kappa

%%% Testing WastonEM
[Mu, Kappa] = EMmWatson_mex(X, Pm, 1);
[cs, Mu_est] = SymDistance(Mu_truth, Mu, Pm);
cs
Kappa

%% Test for 2 clusters parameter estimation.
% Load the testing data
load('WatsonSamples_2C.mat');

%%% Testing WastonEM
[Mu, Kappa, W, logL, CIdx] = EMmWatson(X, Pm, 2);
if(W(1)<0.5)
    [cs1, Mu_est] = SymDistance(Mu1_truth, Mu(1,:), Pm);
    [cs2, Mu_est] = SymDistance(Mu2_truth, Mu(2,:), Pm);
    cs1
    cs2
else
    [cs1, Mu_est] = SymDistance(Mu2_truth, Mu(1,:), Pm);
    [cs2, Mu_est] = SymDistance(Mu1_truth, Mu(2,:), Pm);
    cs1
    cs2
end
Kappa
W
sum(CIdx(:)==1)/numel(CIdx)

%%% Testing WastonEM
[Mu, Kappa, W, logL, CIdx] = EMmWatson_mex(X, Pm, 2);
if(W(1)<0.5)
    [cs1, Mu_est] = SymDistance(Mu1_truth, Mu(1,:), Pm);
    [cs2, Mu_est] = SymDistance(Mu2_truth, Mu(2,:), Pm);
    cs1
    cs2
else
    [cs1, Mu_est] = SymDistance(Mu2_truth, Mu(1,:), Pm);
    [cs2, Mu_est] = SymDistance(Mu1_truth, Mu(2,:), Pm);
    cs1
    cs2
end
Kappa
W
sum(CIdx(:)==2)/numel(CIdx)
