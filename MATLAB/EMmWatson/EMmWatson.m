function [Mu, Kappa, W, logL, CIdx] = EMmWatson(X, Pm, Num_of_clusters)
% This is the EM algorithm for estimating the parameters of group-invariant
% Watson distribution. It also supports the clustering of
% group-invariant samples. Details of the algorithm can be found in the
% following paper:
%   Yu-Hui Chen, Dennis Wei, Gregory Newstadt, Marc DeGraef, 
%   Jeffrey Simmons, Alfred Hero "Statistical Estimation and Clustering of 
%   Group-invariant Orientation Parameters" *International Conference on 
%   Information Fusion, 2015
% 
% Usage:
%  [Mu, Kappa, W, logL, CIdx]=EMmWatson(X, Pm, Num_of_clusters(optional))
% 
% Inputs:
%   X : A N x 4 matrix which contains the N quaternion samples which need to
%       be clustered or estimated its mean and concentration.
%
%   Pm : A 4 x 4 x M matrx where each of the 4 x 4 submatrix is the
%       symmetry operator in rotation orthnormal matrix form.
%
%   Num_of_clusters : An integer which specify the number of clusters in
%       the samples.
%   
%  
% Outputs:
%   Mu : A Num_of_clusters x 4 matrix which contains the estimated means of
%       each cluster in quaternion.
%
%   Kappa : A Num_of_clusters x 1 matrix which contains the estimated kappa
%       of each cluster.
%
%   W : A Num_of_clusters x 1 matrix which contains the mixing weight of
%       the clusters.
%
%   logL : The log-likelihood for the samples of the estimated model.
%
%   CIdx : The N x 1 matrix which indicates the cluster of each sample it
%       belongs to.
%  
% Function is written by Yu-Hui Chen, University of Michigan

%%% Duplicate the Euler Angles
N = size(X,1);
p = size(X,2);
M = size(Pm,3);

% Precompute the xAp, yAp for invAp_fast
global xDp;
global yDp;
    
if(nargin<3)
    Num_of_clusters=1;
end
Num_of_init = 5;

Mu_All = zeros(Num_of_clusters, p, Num_of_init);
Kappa_All = zeros(Num_of_clusters, Num_of_init);
W_All = zeros(Num_of_clusters, Num_of_init);
L_All = zeros(Num_of_init,1);
R_All = zeros(N, Num_of_clusters, Num_of_init);

trial = 1;
while(trial<=Num_of_init)
    % Initialize Mu and Kappa
    R = zeros(N,M,Num_of_clusters);
    Mu = zeros(Num_of_clusters, p);
    for clu = 1:Num_of_clusters
        mu = randn(1,p);
        mu = mu/norm(mu,2);
        Mu(clu,:) = mu(:)';
    end
    Kappa = 100*ones(Num_of_clusters,1);
    W = (1/Num_of_clusters)*ones(Num_of_clusters,1);
    Alpha = 1/M*ones(M,1);
    
    Num_of_iteration=30;
    Q=zeros(Num_of_iteration,1);
    L=zeros(Num_of_iteration,1);
    isfound = true;
    for ite=1:Num_of_iteration
        %%% E-step
        for clu = 1:Num_of_clusters
            mu = Mu(clu,:);
            kappa = Kappa(clu);
            w = W(clu);
            for j=1:M
                R(:,j,clu) = w*Alpha(j)*WatsonDensity(X, Pm(:,:,j)*mu(:), kappa)+eps;
            end
        end
        % Normalization
        Rdenom = sum(sum(R,3),2);
        R = R ./ repmat(Rdenom, [1,M,Num_of_clusters]);

        % M-step
        % estimate W
        W = squeeze(sum(sum(R,1),2));
        W = W / sum(W(:));
            
        if(any(W<0.001))
            isfound=false;
            break;
        end
        %%% Estimate mu
        for clu = 1:Num_of_clusters
            % Calculate Scatter Matrix T
            T = zeros(p,p);
            for j=1:M
                rX = repmat(sqrt(R(:,j,clu)), [1,p]).*X;
                rXXT = rX'*rX;
                T = T + Pm(:,:,j)'*rXXT*Pm(:,:,j);
            end
            T = T / (N*W(clu));
            [V, D] = eig(T);
            lambda = diag(D);
            [yy, dd] = sort(lambda, 'descend');
            
            Mu_1 = V(:,dd(1));
            Mu_p = V(:,dd(end));
            
            %%% Estimate Kappa
            TT = Mu_1(:)'*T*Mu_1(:);
            Kappa_tmp = invDp(real(TT), xDp, yDp);
            Mu_tmp = Mu_1;
            if(Kappa_tmp < 0)
                %%% The other case, need to use Mu_p
                TT = Mu_p(:)'*T*Mu_p(:);
                Kappa_tmp = invDp(real(TT), xDp, yDp);
                Mu_tmp = Mu_p;
                if (Kappa_tmp > 0)
                    Kappa = zeros(Num_of_clusters,1);
                    Mu = zeros(Num_of_clusters, p);
                    isfound = false;
                    break;
                end
            end  
            Mu(clu,:) = real(Mu_tmp(:)');
            Kappa(clu) = Kappa_tmp;
        end
        if(~isfound)
            break;
        end
       
        Kappa(Kappa>710) = 710;
        % Calculate the Q function
        Phi = zeros(N,M,Num_of_clusters);
        for clu = 1:Num_of_clusters
            for j=1:M
                Phi(:,j,clu) = W(clu)*Alpha(j)*WatsonDensity(X, Pm(:,:,j)*Mu(clu,:)', Kappa(clu));
            end
        end
        
        L(ite) = sum(log(sum(sum(Phi,3),2)));
        Q(ite) = sum(sum(sum(R.*log(Phi+eps))));
        
        if(ite>=2)
            if(abs(Q(ite)-Q(ite-1))<0.05)
                break;
            end
        end
    end
    if(isfound)
        % update the containers
        Mu_All(:,:,trial) = Mu;
        Kappa_All(:,trial) = Kappa(:);
        W_All(:,trial) = W(:);
        L_All(trial) = L(ite);
        R_All(:,:,trial) = squeeze(sum(R,2));
        trial = trial + 1;
    end
end

[yy, dd] = max(L_All);
Mu=Mu_All(:,:,dd);
Kappa=Kappa_All(:,dd);
W = W_All(:,dd);
logL = L_All(dd);
if(Num_of_clusters>1)
    [yy,CIdx] = max(squeeze(R_All(:,:,dd)),[],2);
else
    CIdx = ones(N,1);
end

end