function [y] = Dp(x, p)
    y = kummer(3/2,p/2+1,x) ./ (p*kummer(1/2,p/2,x));
end