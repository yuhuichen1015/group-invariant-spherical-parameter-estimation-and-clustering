function [Out] = ReadMtx_bool(filename)
    fid = fopen(filename, 'r');
    if(fid<0)
        error('File Not Exist');
    end
    Sz = fscanf(fid, '%d');
    Out = zeros([Sz(1) Sz(2) Sz(3)]);
    for k=1:Sz(3)
        for i=1:Sz(1)
            for j=1:Sz(2)
                Out(i,j,k) = fread(fid, 1, 'uchar');
            end
        end
    end
    Out = squeeze(Out);
    fclose(fid);
    
end