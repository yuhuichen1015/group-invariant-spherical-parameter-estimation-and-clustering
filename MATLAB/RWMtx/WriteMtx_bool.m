function WriteMtx_bool(filename, Mtx)
    Sz = size(Mtx);
    fid = fopen(filename, 'w');
    if(numel(Sz)==2)
        Sz(3) = 1;
    end
    % Print the size of the matrix
    fprintf(fid, '%d %d %d\n', Sz(1), Sz(2), Sz(3));
    Mtx = logical(Mtx);
    % write the content of the matrix
    for k=1:Sz(3)
        for i=1:Sz(1)
            for j=1:Sz(2)
                fwrite(fid, Mtx(i,j,k), 'uchar');
            end
        end
    end
    fclose(fid);
end