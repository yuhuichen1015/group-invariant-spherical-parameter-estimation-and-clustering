function [Out] = ReadMtx_double(filename)
    fid = fopen(filename, 'r');
    if(fid<0)
        error('File Not Exist');
    end
    Sz = fscanf(fid, '%d');
    Out = zeros([Sz(1) Sz(2) Sz(3)]);
    for k=1:Sz(3)
        for j=1:Sz(2)
            for i=1:Sz(1)
                Out(i,j,k) = fread(fid, 1, 'double');
            end
        end
    end
    Out = squeeze(Out);
    fclose(fid);
    
end