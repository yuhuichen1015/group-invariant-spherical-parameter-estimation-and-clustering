#!/bin/bash

# Set up the bin directory
BIN_PATH='./bin'
TESTDATA_PATH='./TestCases/Data'

# Define the function to check whether the element is in the array.
containsElement () {
  local e
  for e in "${@:2}"; do [[ "$e" == "$1" ]] && return 1; done
  return 0
}

if [ $# -eq 0 ]; then
	echo "Executing all test cases..."
	testcases=("RWMtx" "TransformToRef" "TransformToFZ_cubic" "EMmVMF" "EMmWatson")
else
	echo "Executing $1"
	testcases=$1
fi

# Clean all the log files
containsElement "clean" "${testcases[@]}"
if [ $? -eq 1 ]; then
	rm *.log
	echo "All log files are removed."
fi


# Execute the test case for RWMtx
testname="RWMtx"
containsElement "${testname}" "${testcases[@]}"
if [ $? -eq 1 ]; then
	Binary="$BIN_PATH/Bin/${testname}_test"
	Command="$Binary $TESTDATA_PATH/MtxSample.mtx" 
	eval "$Command > ${testname}_test.log"
	Command="$Binary $TESTDATA_PATH/MtxSample3.mtx" 
	eval "$Command >> ${testname}_test.log"
	echo "Testing result is in ${testname}_test.log"
fi

# Execute the test case for TransformToRef
testname="TransformToRef"
containsElement "${testname}" "${testcases[@]}"
if [ $? -eq 1 ]; then
	Binary="$BIN_PATH/Bin/${testname}_test"
	Command="$Binary $TESTDATA_PATH/QuatRef.mtx $TESTDATA_PATH/Quat.mtx $TESTDATA_PATH/Pm.mtx" 
	eval "$Command > ${testname}_test.log"
	echo "Testing result is in ${testname}_test.log"
fi

# Execute the test case for TransformToFZ_cubic
testname="TransformToFZ_cubic"
containsElement "${testname}" "${testcases[@]}"
if [ $? -eq 1 ]; then
	Binary="$BIN_PATH/Bin/${testname}_test"
	Command="$Binary $TESTDATA_PATH/Quat.mtx" 
	eval "$Command > ${testname}_test.log"
	echo "Testing result is in ${testname}_test.log"
fi

# Execute the test case for EMmVMF
testname="EMmVMF"
containsElement "${testname}" "${testcases[@]}"
if [ $? -eq 1 ]; then
	Binary="$BIN_PATH/Bin/${testname}_test"
	Command="$Binary ${TESTDATA_PATH}/VMFSamples_1C.mtx ${TESTDATA_PATH}/Pm.mtx 1" 
	eval "$Command > ${testname}_test.log"
	Command="$Binary ${TESTDATA_PATH}/VMFSamples_2C.mtx ${TESTDATA_PATH}/Pm.mtx 2" 
	eval "$Command >> ${testname}_test.log"
	echo "Testing result is in ${testname}_test.log"
fi

# Execute the test case for EMmWatson
testname="EMmWatson"
containsElement "${testname}" "${testcases[@]}"
if [ $? -eq 1 ]; then
	Binary="$BIN_PATH/Bin/${testname}_test"
	Command="$Binary ${TESTDATA_PATH}/WatsonSamples_1C.mtx ${TESTDATA_PATH}/Pm.mtx 1" 
	eval "$Command > ${testname}_test.log"
	Command="$Binary ${TESTDATA_PATH}/WatsonSamples_2C.mtx ${TESTDATA_PATH}/Pm.mtx 2" 
	eval "$Command >> ${testname}_test.log"
	echo "Testing result is in ${testname}_test.log"
fi
