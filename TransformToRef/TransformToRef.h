//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: TransformToRef.h
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 28-Jul-2015 12:58:24
//
#ifndef __TRANSFORMTOREF_H__
#define __TRANSFORMTOREF_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "TransformToRef_types.h"

// Function Declarations
extern void TransformToRef(const emxArray_real_T *QuatRef, const emxArray_real_T
  *Quat, const emxArray_real_T *Pm, emxArray_real_T *QuatT);

#endif

//
// File trailer for TransformToRef.h
//
// [EOF]
//
