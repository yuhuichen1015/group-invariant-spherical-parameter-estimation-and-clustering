//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: TransformToRef.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 28-Jul-2015 12:58:24
//

// Include Files
#include "rt_nonfinite.h"
#include "TransformToRef.h"
#include "TransformToRef_emxutil.h"

// Function Definitions

//
// Arguments    : const emxArray_real_T *QuatRef
//                const emxArray_real_T *Quat
//                const emxArray_real_T *Pm
//                emxArray_real_T *QuatT
// Return Type  : void
//
void TransformToRef(const emxArray_real_T *QuatRef, const emxArray_real_T *Quat,
                    const emxArray_real_T *Pm, emxArray_real_T *QuatT)
{
  unsigned int uv0[2];
  int n;
  int ixstart;
  int M;
  int b_n;
  emxArray_real_T *sym;
  emxArray_real_T *y;
  int j;
  double b_y[4];
  double mtmp;
  int m;
  int itmp;
  int br;
  int ia;
  boolean_T exitg1;
  double b_Pm[4];
  for (n = 0; n < 2; n++) {
    uv0[n] = (unsigned int)Quat->size[n];
  }

  n = QuatT->size[0] * QuatT->size[1];
  QuatT->size[0] = (int)uv0[0];
  QuatT->size[1] = 4;
  emxEnsureCapacity((emxArray__common *)QuatT, n, (int)sizeof(double));
  ixstart = (int)uv0[0] << 2;
  for (n = 0; n < ixstart; n++) {
    QuatT->data[n] = 0.0;
  }

  M = Pm->size[2] - 1;

  //  Transforming all angles to the be closest to the first one
  b_n = 0;
  emxInit_real_T(&sym, 1);
  emxInit_real_T(&y, 1);
  while (b_n <= Quat->size[0] - 1) {
    n = sym->size[0];
    sym->size[0] = M + 1;
    emxEnsureCapacity((emxArray__common *)sym, n, (int)sizeof(double));
    for (n = 0; n <= M; n++) {
      sym->data[n] = 0.0;
    }

    for (j = 0; j <= M; j++) {
      for (n = 0; n < 4; n++) {
        b_y[n] = 0.0;
        for (ixstart = 0; ixstart < 4; ixstart++) {
          mtmp = b_y[n] + Pm->data[(ixstart + Pm->size[0] * n) + Pm->size[0] *
            Pm->size[1] * j] * Quat->data[b_n + Quat->size[0] * ixstart];
          b_y[n] = mtmp;
        }
      }

      uv0[0] = (unsigned int)QuatRef->size[0];
      m = QuatRef->size[0];
      n = y->size[0];
      y->size[0] = (int)uv0[0];
      emxEnsureCapacity((emxArray__common *)y, n, (int)sizeof(double));
      ixstart = (int)uv0[0];
      for (n = 0; n < ixstart; n++) {
        y->data[n] = 0.0;
      }

      if (QuatRef->size[0] == 0) {
      } else {
        ixstart = 0;
        while ((m > 0) && (ixstart <= 0)) {
          for (itmp = 1; itmp <= m; itmp++) {
            y->data[itmp - 1] = 0.0;
          }

          ixstart = m;
        }

        br = 0;
        ixstart = 0;
        while ((m > 0) && (ixstart <= 0)) {
          ixstart = -1;
          for (n = br; n + 1 <= br + 4; n++) {
            if (b_y[n] != 0.0) {
              ia = ixstart;
              for (itmp = 0; itmp + 1 <= m; itmp++) {
                ia++;
                y->data[itmp] += b_y[n] * QuatRef->data[ia];
              }
            }

            ixstart += m;
          }

          br += 4;
          ixstart = m;
        }
      }

      sym->data[j] = y->data[0];
    }

    uv0[0] = (unsigned int)sym->size[0];
    n = y->size[0];
    y->size[0] = (int)uv0[0];
    emxEnsureCapacity((emxArray__common *)y, n, (int)sizeof(double));
    for (ixstart = 0; ixstart < sym->size[0]; ixstart++) {
      y->data[ixstart] = fabs(sym->data[ixstart]);
    }

    ixstart = 1;
    n = y->size[0];
    mtmp = y->data[0];
    itmp = 0;
    if (y->size[0] > 1) {
      if (rtIsNaN(y->data[0])) {
        ia = 1;
        exitg1 = false;
        while ((!exitg1) && (ia + 1 <= n)) {
          ixstart = ia + 1;
          if (!rtIsNaN(y->data[ia])) {
            mtmp = y->data[ia];
            itmp = ia;
            exitg1 = true;
          } else {
            ia++;
          }
        }
      }

      if (ixstart < y->size[0]) {
        while (ixstart + 1 <= n) {
          if (y->data[ixstart] > mtmp) {
            mtmp = y->data[ixstart];
            itmp = ixstart;
          }

          ixstart++;
        }
      }
    }

    if (sym->data[itmp] < 0.0) {
      for (n = 0; n < 4; n++) {
        b_Pm[n] = 0.0;
        for (ixstart = 0; ixstart < 4; ixstart++) {
          mtmp = b_Pm[n] + Pm->data[(ixstart + Pm->size[0] * n) + Pm->size[0] *
            Pm->size[1] * itmp] * Quat->data[b_n + Quat->size[0] * ixstart];
          b_Pm[n] = mtmp;
        }
      }

      for (n = 0; n < 4; n++) {
        QuatT->data[b_n + QuatT->size[0] * n] = -b_Pm[n];
      }
    } else {
      for (n = 0; n < 4; n++) {
        b_Pm[n] = 0.0;
        for (ixstart = 0; ixstart < 4; ixstart++) {
          mtmp = b_Pm[n] + Pm->data[(ixstart + Pm->size[0] * n) + Pm->size[0] *
            Pm->size[1] * itmp] * Quat->data[b_n + Quat->size[0] * ixstart];
          b_Pm[n] = mtmp;
        }
      }

      for (n = 0; n < 4; n++) {
        QuatT->data[b_n + QuatT->size[0] * n] = b_Pm[n];
      }
    }

    b_n++;
  }

  emxFree_real_T(&y);
  emxFree_real_T(&sym);
}

//
// File trailer for TransformToRef.cpp
//
// [EOF]
//
