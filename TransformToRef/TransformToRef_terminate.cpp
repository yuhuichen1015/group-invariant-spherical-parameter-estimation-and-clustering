//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: TransformToRef_terminate.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 28-Jul-2015 12:58:24
//

// Include Files
#include "rt_nonfinite.h"
#include "TransformToRef.h"
#include "TransformToRef_terminate.h"

// Function Definitions

//
// Arguments    : void
// Return Type  : void
//
void TransformToRef_terminate()
{
  // (no terminate code required)
}

//
// File trailer for TransformToRef_terminate.cpp
//
// [EOF]
//
