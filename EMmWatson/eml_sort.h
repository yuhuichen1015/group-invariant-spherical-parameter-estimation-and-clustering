//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: eml_sort.h
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:05:25
//
#ifndef __EML_SORT_H__
#define __EML_SORT_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "EMmWatson_types.h"

// Function Declarations
extern void eml_sort(creal_T x[4], int idx[4]);

#endif

//
// File trailer for eml_sort.h
//
// [EOF]
//
