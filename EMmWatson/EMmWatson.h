//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: EMmWatson.h
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:05:25
//
#ifndef __EMMWATSON_H__
#define __EMMWATSON_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "EMmWatson_types.h"

// Function Declarations
extern void EMmWatson(const emxArray_real_T *X, const emxArray_real_T *Pm,
                      double Num_of_clusters, emxArray_real_T *Mu,
                      emxArray_real_T *Kappa, emxArray_real_T *W, double *logL,
                      emxArray_real_T *CIdx);

#endif

//
// File trailer for EMmWatson.h
//
// [EOF]
//
