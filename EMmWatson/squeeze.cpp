//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: squeeze.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:05:25
//

// Include Files
#include "rt_nonfinite.h"
#include "EMmWatson.h"
#include "squeeze.h"
#include "EMmWatson_emxutil.h"

// Function Definitions

//
// Arguments    : const emxArray_real_T *a
//                emxArray_real_T *b
// Return Type  : void
//
void b_squeeze(const emxArray_real_T *a, emxArray_real_T *b)
{
  int k;
  int i2;
  int sqsz[3];
  k = 3;
  while ((k > 2) && (a->size[2] == 1)) {
    k = 2;
  }

  if (k <= 2) {
    k = a->size[0];
    i2 = b->size[0] * b->size[1];
    b->size[0] = k;
    b->size[1] = 1;
    emxEnsureCapacity((emxArray__common *)b, i2, (int)sizeof(double));
    i2 = a->size[0] * a->size[2];
    for (k = 0; k + 1 <= i2; k++) {
      b->data[k] = a->data[k];
    }
  } else {
    for (i2 = 0; i2 < 3; i2++) {
      sqsz[i2] = 1;
    }

    k = 0;
    if (a->size[0] != 1) {
      sqsz[0] = a->size[0];
      k = 1;
    }

    if (a->size[2] != 1) {
      sqsz[k] = a->size[2];
    }

    i2 = b->size[0] * b->size[1];
    b->size[0] = sqsz[0];
    b->size[1] = sqsz[1];
    emxEnsureCapacity((emxArray__common *)b, i2, (int)sizeof(double));
    i2 = a->size[0] * a->size[2];
    for (k = 0; k + 1 <= i2; k++) {
      b->data[k] = a->data[k];
    }
  }
}

//
// Arguments    : const emxArray_real_T *a
//                emxArray_real_T *b
// Return Type  : void
//
void squeeze(const emxArray_real_T *a, emxArray_real_T *b)
{
  int k;
  int sqsz[3];
  k = 3;
  while ((k > 2) && (a->size[2] == 1)) {
    k = 2;
  }

  if (k <= 2) {
    k = b->size[0];
    b->size[0] = 1;
    emxEnsureCapacity((emxArray__common *)b, k, (int)sizeof(double));
    k = 1;
    while (k <= a->size[2]) {
      b->data[0] = a->data[0];
      k = 2;
    }
  } else {
    for (k = 0; k < 3; k++) {
      sqsz[k] = 1;
    }

    if (a->size[2] != 1) {
      sqsz[0] = a->size[2];
    }

    k = b->size[0];
    b->size[0] = sqsz[0];
    emxEnsureCapacity((emxArray__common *)b, k, (int)sizeof(double));
    for (k = 0; k + 1 <= a->size[2]; k++) {
      b->data[k] = a->data[k];
    }
  }
}

//
// File trailer for squeeze.cpp
//
// [EOF]
//
