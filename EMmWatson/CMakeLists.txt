
project(EMmWatson)

set(EMmWatson_SRCS
${EMmWatson_SOURCE_DIR}/any.cpp
${EMmWatson_SOURCE_DIR}/any.h
${EMmWatson_SOURCE_DIR}/diag.cpp
${EMmWatson_SOURCE_DIR}/diag.h
${EMmWatson_SOURCE_DIR}/eig.cpp
${EMmWatson_SOURCE_DIR}/eig.h
${EMmWatson_SOURCE_DIR}/eml_rand_mt19937ar_stateful.cpp
${EMmWatson_SOURCE_DIR}/eml_rand_mt19937ar_stateful.h
${EMmWatson_SOURCE_DIR}/eml_sort.cpp
${EMmWatson_SOURCE_DIR}/eml_sort.h
${EMmWatson_SOURCE_DIR}/EMmWatson.cpp
${EMmWatson_SOURCE_DIR}/EMmWatson_data.cpp
${EMmWatson_SOURCE_DIR}/EMmWatson_data.h
${EMmWatson_SOURCE_DIR}/EMmWatson_emxAPI.cpp
${EMmWatson_SOURCE_DIR}/EMmWatson_emxAPI.h
${EMmWatson_SOURCE_DIR}/EMmWatson_emxutil.cpp
${EMmWatson_SOURCE_DIR}/EMmWatson_emxutil.h
${EMmWatson_SOURCE_DIR}/EMmWatson.h
${EMmWatson_SOURCE_DIR}/EMmWatson_initialize.cpp
${EMmWatson_SOURCE_DIR}/EMmWatson_initialize.h
${EMmWatson_SOURCE_DIR}/EMmWatson_rtwutil.cpp
${EMmWatson_SOURCE_DIR}/EMmWatson_rtwutil.h
${EMmWatson_SOURCE_DIR}/EMmWatson_terminate.cpp
${EMmWatson_SOURCE_DIR}/EMmWatson_terminate.h
${EMmWatson_SOURCE_DIR}/EMmWatson_types.h
${EMmWatson_SOURCE_DIR}/invDp.cpp
${EMmWatson_SOURCE_DIR}/invDp.h
${EMmWatson_SOURCE_DIR}/log.cpp
${EMmWatson_SOURCE_DIR}/log.h
${EMmWatson_SOURCE_DIR}/norm.cpp
${EMmWatson_SOURCE_DIR}/norm.h
${EMmWatson_SOURCE_DIR}/randn.cpp
${EMmWatson_SOURCE_DIR}/randn.h
${EMmWatson_SOURCE_DIR}/rdivide.cpp
${EMmWatson_SOURCE_DIR}/rdivide.h
${EMmWatson_SOURCE_DIR}/repmat.cpp
${EMmWatson_SOURCE_DIR}/repmat.h
${EMmWatson_SOURCE_DIR}/rt_defines.h
${EMmWatson_SOURCE_DIR}/rtGetInf.cpp
${EMmWatson_SOURCE_DIR}/rtGetInf.h
${EMmWatson_SOURCE_DIR}/rtGetNaN.cpp
${EMmWatson_SOURCE_DIR}/rtGetNaN.h
${EMmWatson_SOURCE_DIR}/rt_nonfinite.cpp
${EMmWatson_SOURCE_DIR}/rt_nonfinite.h
${EMmWatson_SOURCE_DIR}/rtwtypes.h
${EMmWatson_SOURCE_DIR}/sqrt.cpp
${EMmWatson_SOURCE_DIR}/sqrt.h
${EMmWatson_SOURCE_DIR}/squeeze.cpp
${EMmWatson_SOURCE_DIR}/squeeze.h
${EMmWatson_SOURCE_DIR}/sum.cpp
${EMmWatson_SOURCE_DIR}/sum.h
${EMmWatson_SOURCE_DIR}/WatsonDensity.cpp
${EMmWatson_SOURCE_DIR}/WatsonDensity.h
)

add_library(EMmWatson ${EMmWatson_SRCS})






