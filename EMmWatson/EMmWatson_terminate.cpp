//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: EMmWatson_terminate.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:05:25
//

// Include Files
#include "rt_nonfinite.h"
#include "EMmWatson.h"
#include "EMmWatson_terminate.h"

// Function Definitions

//
// Arguments    : void
// Return Type  : void
//
void EMmWatson_terminate()
{
  // (no terminate code required)
}

//
// File trailer for EMmWatson_terminate.cpp
//
// [EOF]
//
