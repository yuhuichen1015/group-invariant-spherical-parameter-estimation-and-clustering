//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: diag.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:05:25
//

// Include Files
#include "rt_nonfinite.h"
#include "EMmWatson.h"
#include "diag.h"

// Function Definitions

//
// Arguments    : const creal_T v[16]
//                creal_T d[4]
// Return Type  : void
//
void diag(const creal_T v[16], creal_T d[4])
{
  int j;
  for (j = 0; j < 4; j++) {
    d[j] = v[j * 5];
  }
}

//
// File trailer for diag.cpp
//
// [EOF]
//
