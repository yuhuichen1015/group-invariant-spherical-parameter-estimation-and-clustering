//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: sum.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:05:25
//

// Include Files
#include "rt_nonfinite.h"
#include "EMmWatson.h"
#include "sum.h"
#include "EMmWatson_emxutil.h"

// Function Definitions

//
// Arguments    : const emxArray_real_T *x
//                emxArray_real_T *y
// Return Type  : void
//
void b_sum(const emxArray_real_T *x, emxArray_real_T *y)
{
  unsigned int sz[2];
  int vstride;
  int iy;
  int ixstart;
  int j;
  int ix;
  double s;
  int k;
  for (vstride = 0; vstride < 2; vstride++) {
    sz[vstride] = (unsigned int)x->size[vstride];
  }

  vstride = y->size[0];
  y->size[0] = (int)sz[0];
  emxEnsureCapacity((emxArray__common *)y, vstride, (int)sizeof(double));
  if ((x->size[0] == 0) || (x->size[1] == 0)) {
    vstride = y->size[0];
    y->size[0] = (int)sz[0];
    emxEnsureCapacity((emxArray__common *)y, vstride, (int)sizeof(double));
    iy = (int)sz[0];
    for (vstride = 0; vstride < iy; vstride++) {
      y->data[vstride] = 0.0;
    }
  } else {
    vstride = x->size[0];
    iy = -1;
    ixstart = -1;
    for (j = 1; j <= vstride; j++) {
      ixstart++;
      ix = ixstart;
      s = x->data[ixstart];
      for (k = 2; k <= x->size[1]; k++) {
        ix += vstride;
        s += x->data[ix];
      }

      iy++;
      y->data[iy] = s;
    }
  }
}

//
// Arguments    : const emxArray_real_T *x
//                emxArray_real_T *y
// Return Type  : void
//
void c_sum(const emxArray_real_T *x, emxArray_real_T *y)
{
  unsigned int sz[3];
  int ix;
  int k;
  int npages;
  int iy;
  int i;
  double s;
  for (ix = 0; ix < 3; ix++) {
    sz[ix] = (unsigned int)x->size[ix];
  }

  ix = y->size[0] * y->size[1] * y->size[2];
  y->size[0] = 1;
  y->size[1] = (int)sz[1];
  y->size[2] = (int)sz[2];
  emxEnsureCapacity((emxArray__common *)y, ix, (int)sizeof(double));
  if ((x->size[0] == 0) || (x->size[1] == 0) || (x->size[2] == 0)) {
    ix = y->size[0] * y->size[1] * y->size[2];
    y->size[0] = 1;
    emxEnsureCapacity((emxArray__common *)y, ix, (int)sizeof(double));
    ix = y->size[0] * y->size[1] * y->size[2];
    y->size[1] = (int)sz[1];
    emxEnsureCapacity((emxArray__common *)y, ix, (int)sizeof(double));
    ix = y->size[0] * y->size[1] * y->size[2];
    y->size[2] = (int)sz[2];
    emxEnsureCapacity((emxArray__common *)y, ix, (int)sizeof(double));
    k = (int)sz[1] * (int)sz[2];
    for (ix = 0; ix < k; ix++) {
      y->data[ix] = 0.0;
    }
  } else {
    npages = 1;
    k = 3;
    while ((k > 2) && (x->size[2] == 1)) {
      k = 2;
    }

    for (ix = 2; ix <= k; ix++) {
      npages *= x->size[ix - 1];
    }

    ix = 0;
    iy = -1;
    for (i = 1; i <= npages; i++) {
      k = ix;
      ix++;
      s = x->data[k];
      for (k = 2; k <= x->size[0]; k++) {
        ix++;
        s += x->data[ix - 1];
      }

      iy++;
      y->data[iy] = s;
    }
  }
}

//
// Arguments    : const emxArray_real_T *x
//                emxArray_real_T *y
// Return Type  : void
//
void d_sum(const emxArray_real_T *x, emxArray_real_T *y)
{
  unsigned int sz[3];
  int ix;
  int k;
  int npages;
  int iy;
  int i;
  double s;
  for (ix = 0; ix < 3; ix++) {
    sz[ix] = (unsigned int)x->size[ix];
  }

  ix = y->size[0] * y->size[1] * y->size[2];
  y->size[0] = 1;
  y->size[1] = 1;
  y->size[2] = (int)sz[2];
  emxEnsureCapacity((emxArray__common *)y, ix, (int)sizeof(double));
  if ((x->size[1] == 0) || (x->size[2] == 0)) {
    ix = y->size[0] * y->size[1] * y->size[2];
    y->size[0] = 1;
    y->size[1] = 1;
    emxEnsureCapacity((emxArray__common *)y, ix, (int)sizeof(double));
    ix = y->size[0] * y->size[1] * y->size[2];
    y->size[2] = (int)sz[2];
    emxEnsureCapacity((emxArray__common *)y, ix, (int)sizeof(double));
    k = (int)sz[2];
    for (ix = 0; ix < k; ix++) {
      y->data[ix] = 0.0;
    }
  } else {
    npages = 1;
    k = 3;
    while ((k > 2) && (x->size[2] == 1)) {
      k = 2;
    }

    ix = 3;
    while (ix <= k) {
      npages *= x->size[2];
      ix = 4;
    }

    ix = 0;
    iy = -1;
    for (i = 1; i <= npages; i++) {
      k = ix;
      ix++;
      s = x->data[k];
      for (k = 2; k <= x->size[1]; k++) {
        ix++;
        s += x->data[ix - 1];
      }

      iy++;
      y->data[iy] = s;
    }
  }
}

//
// Arguments    : const emxArray_real_T *x
// Return Type  : double
//
double e_sum(const emxArray_real_T *x)
{
  double y;
  int k;
  if (x->size[0] == 0) {
    y = 0.0;
  } else {
    y = x->data[0];
    for (k = 2; k <= x->size[0]; k++) {
      y += x->data[k - 1];
    }
  }

  return y;
}

//
// Arguments    : const emxArray_real_T *x
//                emxArray_real_T *y
// Return Type  : void
//
void f_sum(const emxArray_real_T *x, emxArray_real_T *y)
{
  unsigned int sz[3];
  int ix;
  int k;
  int npages;
  int iy;
  int i;
  double s;
  for (ix = 0; ix < 3; ix++) {
    sz[ix] = (unsigned int)x->size[ix];
  }

  ix = y->size[0] * y->size[1] * y->size[2];
  y->size[0] = 1;
  y->size[1] = (int)sz[1];
  y->size[2] = (int)sz[2];
  emxEnsureCapacity((emxArray__common *)y, ix, (int)sizeof(double));
  if ((x->size[0] == 0) || (x->size[1] == 0) || (x->size[2] == 0)) {
    ix = y->size[0] * y->size[1] * y->size[2];
    y->size[0] = 1;
    emxEnsureCapacity((emxArray__common *)y, ix, (int)sizeof(double));
    ix = y->size[0] * y->size[1] * y->size[2];
    y->size[1] = (int)sz[1];
    emxEnsureCapacity((emxArray__common *)y, ix, (int)sizeof(double));
    ix = y->size[0] * y->size[1] * y->size[2];
    y->size[2] = (int)sz[2];
    emxEnsureCapacity((emxArray__common *)y, ix, (int)sizeof(double));
    k = (int)sz[1] * (int)sz[2];
    for (ix = 0; ix < k; ix++) {
      y->data[ix] = 0.0;
    }
  } else {
    npages = 1;
    k = 3;
    while ((k > 2) && (x->size[2] == 1)) {
      k = 2;
    }

    for (ix = 2; ix <= k; ix++) {
      npages *= x->size[ix - 1];
    }

    ix = -1;
    iy = -1;
    for (i = 1; i <= npages; i++) {
      k = ix + 2;
      ix++;
      s = x->data[k - 1];
      for (k = 2; k <= x->size[0]; k++) {
        ix++;
        s += x->data[ix];
      }

      iy++;
      y->data[iy] = s;
    }
  }
}

//
// Arguments    : const emxArray_real_T *x
//                emxArray_real_T *y
// Return Type  : void
//
void g_sum(const emxArray_real_T *x, emxArray_real_T *y)
{
  unsigned int sz[3];
  int ix;
  int k;
  int npages;
  int iy;
  int i;
  double s;
  for (ix = 0; ix < 3; ix++) {
    sz[ix] = (unsigned int)x->size[ix];
  }

  ix = y->size[0] * y->size[1] * y->size[2];
  y->size[0] = 1;
  y->size[1] = 1;
  y->size[2] = (int)sz[2];
  emxEnsureCapacity((emxArray__common *)y, ix, (int)sizeof(double));
  if ((x->size[1] == 0) || (x->size[2] == 0)) {
    ix = y->size[0] * y->size[1] * y->size[2];
    y->size[0] = 1;
    y->size[1] = 1;
    emxEnsureCapacity((emxArray__common *)y, ix, (int)sizeof(double));
    ix = y->size[0] * y->size[1] * y->size[2];
    y->size[2] = (int)sz[2];
    emxEnsureCapacity((emxArray__common *)y, ix, (int)sizeof(double));
    k = (int)sz[2];
    for (ix = 0; ix < k; ix++) {
      y->data[ix] = 0.0;
    }
  } else {
    npages = 1;
    k = 3;
    while ((k > 2) && (x->size[2] == 1)) {
      k = 2;
    }

    ix = 3;
    while (ix <= k) {
      npages *= x->size[2];
      ix = 4;
    }

    ix = -1;
    iy = -1;
    for (i = 1; i <= npages; i++) {
      k = ix + 2;
      ix++;
      s = x->data[k - 1];
      for (k = 2; k <= x->size[1]; k++) {
        ix++;
        s += x->data[ix];
      }

      iy++;
      y->data[iy] = s;
    }
  }
}

//
// Arguments    : const emxArray_real_T *x
// Return Type  : double
//
double h_sum(const emxArray_real_T *x)
{
  double y;
  int k;
  if (x->size[2] == 0) {
    y = 0.0;
  } else {
    y = x->data[0];
    for (k = 2; k <= x->size[2]; k++) {
      y += x->data[k - 1];
    }
  }

  return y;
}

//
// Arguments    : const emxArray_real_T *x
//                emxArray_real_T *y
// Return Type  : void
//
void i_sum(const emxArray_real_T *x, emxArray_real_T *y)
{
  unsigned int sz[3];
  int iy;
  int ix;
  int vstride;
  int npages;
  int k;
  int i;
  int ixstart;
  int j;
  double s;
  for (iy = 0; iy < 3; iy++) {
    sz[iy] = (unsigned int)x->size[iy];
  }

  iy = y->size[0] * y->size[1] * y->size[2];
  y->size[0] = (int)sz[0];
  y->size[1] = 1;
  y->size[2] = (int)sz[2];
  emxEnsureCapacity((emxArray__common *)y, iy, (int)sizeof(double));
  if ((x->size[0] == 0) || (x->size[1] == 0) || (x->size[2] == 0)) {
    iy = y->size[0] * y->size[1] * y->size[2];
    y->size[0] = (int)sz[0];
    y->size[1] = 1;
    emxEnsureCapacity((emxArray__common *)y, iy, (int)sizeof(double));
    iy = y->size[0] * y->size[1] * y->size[2];
    y->size[2] = (int)sz[2];
    emxEnsureCapacity((emxArray__common *)y, iy, (int)sizeof(double));
    ix = (int)sz[0] * (int)sz[2];
    for (iy = 0; iy < ix; iy++) {
      y->data[iy] = 0.0;
    }
  } else {
    vstride = x->size[0];
    npages = 1;
    k = 3;
    while ((k > 2) && (x->size[2] == 1)) {
      k = 2;
    }

    iy = 3;
    while (iy <= k) {
      npages *= x->size[2];
      iy = 4;
    }

    ix = 0;
    iy = -1;
    for (i = 1; i <= npages; i++) {
      ixstart = ix;
      for (j = 1; j <= vstride; j++) {
        ixstart++;
        ix = ixstart;
        s = x->data[ixstart - 1];
        for (k = 2; k <= x->size[1]; k++) {
          ix += vstride;
          s += x->data[ix - 1];
        }

        iy++;
        y->data[iy] = s;
      }
    }
  }
}

//
// Arguments    : const emxArray_real_T *x
//                emxArray_real_T *y
// Return Type  : void
//
void sum(const emxArray_real_T *x, emxArray_real_T *y)
{
  unsigned int sz[3];
  int k;
  int vstride;
  int iy;
  int ixstart;
  int j;
  int ix;
  double s;
  for (k = 0; k < 3; k++) {
    sz[k] = (unsigned int)x->size[k];
  }

  k = y->size[0] * y->size[1];
  y->size[0] = (int)sz[0];
  y->size[1] = (int)sz[1];
  emxEnsureCapacity((emxArray__common *)y, k, (int)sizeof(double));
  if ((x->size[0] == 0) || (x->size[1] == 0) || (x->size[2] == 0)) {
    k = y->size[0] * y->size[1];
    y->size[0] = (int)sz[0];
    emxEnsureCapacity((emxArray__common *)y, k, (int)sizeof(double));
    k = y->size[0] * y->size[1];
    y->size[1] = (int)sz[1];
    emxEnsureCapacity((emxArray__common *)y, k, (int)sizeof(double));
    vstride = (int)sz[0] * (int)sz[1];
    for (k = 0; k < vstride; k++) {
      y->data[k] = 0.0;
    }
  } else {
    k = 3;
    while ((k > 2) && (x->size[2] == 1)) {
      k = 2;
    }

    if (3 > k) {
      vstride = x->size[0] * x->size[1] * x->size[2];
    } else {
      vstride = 1;
      for (k = 0; k < 2; k++) {
        vstride *= x->size[k];
      }
    }

    iy = -1;
    ixstart = -1;
    for (j = 1; j <= vstride; j++) {
      ixstart++;
      ix = ixstart;
      s = x->data[ixstart];
      for (k = 2; k <= x->size[2]; k++) {
        ix += vstride;
        s += x->data[ix];
      }

      iy++;
      y->data[iy] = s;
    }
  }
}

//
// File trailer for sum.cpp
//
// [EOF]
//
