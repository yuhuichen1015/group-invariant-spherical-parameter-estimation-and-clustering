//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: EMmWatson_data.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:05:25
//

// Include Files
#include "rt_nonfinite.h"
#include "EMmWatson.h"
#include "EMmWatson_data.h"

// Variable Definitions
unsigned int state[625];

//
// File trailer for EMmWatson_data.cpp
//
// [EOF]
//
