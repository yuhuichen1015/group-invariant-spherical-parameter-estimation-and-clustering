//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: log.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:05:25
//

// Include Files
#include "rt_nonfinite.h"
#include "EMmWatson.h"
#include "log.h"

// Function Definitions

//
// Arguments    : emxArray_real_T *x
// Return Type  : void
//
void b_log(emxArray_real_T *x)
{
  int i4;
  int k;
  i4 = x->size[0];
  for (k = 0; k < i4; k++) {
    x->data[k] = log(x->data[k]);
  }
}

//
// Arguments    : emxArray_real_T *x
// Return Type  : void
//
void c_log(emxArray_real_T *x)
{
  int i5;
  int k;
  i5 = x->size[0] * x->size[1] * x->size[2];
  for (k = 0; k < i5; k++) {
    x->data[k] = log(x->data[k]);
  }
}

//
// File trailer for log.cpp
//
// [EOF]
//
