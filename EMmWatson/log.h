//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: log.h
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:05:25
//
#ifndef __LOG_H__
#define __LOG_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "EMmWatson_types.h"

// Function Declarations
extern void b_log(emxArray_real_T *x);
extern void c_log(emxArray_real_T *x);

#endif

//
// File trailer for log.h
//
// [EOF]
//
