//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: WatsonDensity.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:05:25
//

// Include Files
#include "rt_nonfinite.h"
#include "EMmWatson.h"
#include "WatsonDensity.h"
#include "EMmWatson_emxutil.h"

// Function Definitions

//
// Arguments    : const emxArray_real_T *x
//                const double mu[4]
//                double k
//                emxArray_real_T *y
// Return Type  : void
//
void WatsonDensity(const emxArray_real_T *x, const double mu[4], double k,
                   emxArray_real_T *y)
{
  double term;
  double f;
  double n;
  double an;
  double bn;
  unsigned int unnamed_idx_0;
  int m;
  int cr;
  int ar;
  int ic;
  int br;
  int ia;
  emxArray_real_T *ytmp;

  //  This function estimates the Kummer function with the specified tolerance
  //  the generalized hypergeometric series, noted below.  This solves Kummer's
  //  differential equation:
  //
  //        x*g''(x) + (b - x)*g'(x) - a*g(x) = 0
  //  Default tolerance is tol = 1e-10.  Feel free to change this as needed.
  //  Estimates the value by summing powers of the generalized hypergeometric
  //  series:
  //
  //        sum(n=0-->Inf)[(a)_n*x^n/{(b)_n*n!}
  //
  //  until the specified tolerance is acheived.
  term = k * 0.5 / 2.0;
  f = 1.0 + term;
  n = 1.0;
  an = 0.5;
  bn = 2.0;
  while ((n < 10.0) || (fabs(term) > 1.0E-10)) {
    n++;
    an++;
    bn++;
    term = k * term * an / bn / n;
    f += term;
  }

  //  VERSION INFORMATION
  //  v1 - Written to support only scalar inputs for x
  //  v2 - Changed to support column inputs for x by using the repmat
  //  command and using matrix multiplication to achieve the desired sum
  //
  //  v3 - Credit goes to Ben Petschel for making this suggestion.
  //     The previous method of creating vectors for multiplication to
  //     produce the sum was replaced by a while loop that executes
  //     until a certain tolerance is achieved.  My previous thinking
  //     was avoiding a loop would produce a code that would execute
  //     faster.  Ben pointed out this is not necessarily true, and not
  //     true in this case.  Not only does the while loop used execute
  //     faster for this calculation, but it is also more accurate.
  f = log(1.0 / f);
  unnamed_idx_0 = (unsigned int)x->size[0];
  m = x->size[0];
  cr = y->size[0];
  y->size[0] = (int)unnamed_idx_0;
  emxEnsureCapacity((emxArray__common *)y, cr, (int)sizeof(double));
  ar = (int)unnamed_idx_0;
  for (cr = 0; cr < ar; cr++) {
    y->data[cr] = 0.0;
  }

  if (x->size[0] == 0) {
  } else {
    cr = 0;
    while ((m > 0) && (cr <= 0)) {
      for (ic = 1; ic <= m; ic++) {
        y->data[ic - 1] = 0.0;
      }

      cr = m;
    }

    br = 0;
    cr = 0;
    while ((m > 0) && (cr <= 0)) {
      ar = 0;
      for (cr = br; cr + 1 <= br + 4; cr++) {
        if (mu[cr] != 0.0) {
          ia = ar;
          for (ic = 0; ic + 1 <= m; ic++) {
            ia++;
            y->data[ic] += mu[cr] * x->data[ia - 1];
          }
        }

        ar += m;
      }

      br += 4;
      cr = m;
    }
  }

  c_emxInit_real_T(&ytmp, 1);
  unnamed_idx_0 = (unsigned int)y->size[0];
  cr = ytmp->size[0];
  ytmp->size[0] = (int)unnamed_idx_0;
  emxEnsureCapacity((emxArray__common *)ytmp, cr, (int)sizeof(double));
  for (cr = 0; cr < (int)unnamed_idx_0; cr++) {
    ytmp->data[cr] = y->data[cr] * y->data[cr];
  }

  cr = ytmp->size[0];
  emxEnsureCapacity((emxArray__common *)ytmp, cr, (int)sizeof(double));
  ar = ytmp->size[0];
  for (cr = 0; cr < ar; cr++) {
    ytmp->data[cr] = f + k * ytmp->data[cr];
  }

  cr = y->size[0];
  y->size[0] = ytmp->size[0];
  emxEnsureCapacity((emxArray__common *)y, cr, (int)sizeof(double));
  ar = ytmp->size[0];
  for (cr = 0; cr < ar; cr++) {
    y->data[cr] = ytmp->data[cr];
  }

  for (cr = 0; cr < ytmp->size[0]; cr++) {
    y->data[cr] = exp(y->data[cr]);
  }

  emxFree_real_T(&ytmp);

  //      y = C*exp(k*(x*mu(:)).^2);
}

//
// File trailer for WatsonDensity.cpp
//
// [EOF]
//
