//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: EMmWatson.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:05:25
//

// Include Files
#include "rt_nonfinite.h"
#include "EMmWatson.h"
#include "norm.h"
#include "randn.h"
#include "WatsonDensity.h"
#include "squeeze.h"
#include "sum.h"
#include "EMmWatson_emxutil.h"
#include "repmat.h"
#include "sqrt.h"
#include "log.h"
#include "invDp.h"
#include "eml_sort.h"
#include "diag.h"
#include "eig.h"
#include "any.h"
#include "rdivide.h"

// Function Declarations
static int div_nzp_s32_floor(int numerator, int denominator);

// Function Definitions

//
// Arguments    : int numerator
//                int denominator
// Return Type  : int
//
static int div_nzp_s32_floor(int numerator, int denominator)
{
  int quotient;
  unsigned int absNumerator;
  unsigned int absDenominator;
  boolean_T quotientNeedsNegation;
  unsigned int tempAbsQuotient;
  if (numerator >= 0) {
    absNumerator = (unsigned int)numerator;
  } else {
    absNumerator = (unsigned int)-numerator;
  }

  if (denominator >= 0) {
    absDenominator = (unsigned int)denominator;
  } else {
    absDenominator = (unsigned int)-denominator;
  }

  quotientNeedsNegation = ((numerator < 0) != (denominator < 0));
  tempAbsQuotient = absNumerator / absDenominator;
  if (quotientNeedsNegation) {
    absNumerator %= absDenominator;
    if (absNumerator > 0U) {
      tempAbsQuotient++;
    }
  }

  if (quotientNeedsNegation) {
    quotient = -(int)tempAbsQuotient;
  } else {
    quotient = (int)tempAbsQuotient;
  }

  return quotient;
}

//
// This is the EM algorithm for estimating the parameters of group-invariant
//  Watson distribution. It also supports the clustering of
//  group-invariant samples. Details of the algorithm can be found in the
//  following paper:
//    Yu-Hui Chen, Dennis Wei, Gregory Newstadt, Marc DeGraef,
//    Jeffrey Simmons, Alfred Hero "Statistical Estimation and Clustering of
//    Group-invariant Orientation Parameters" *International Conference on
//    Information Fusion, 2015
//
//  Usage:
//   [Mu, Kappa, W, logL, CIdx]=EMmWatson(X, Pm, Num_of_clusters(optional))
//
//  Inputs:
//    X : A N x 4 matrix which contains the N quaternion samples which need to
//        be clustered or estimated its mean and concentration.
//
//    Pm : A 4 x 4 x M matrx where each of the 4 x 4 submatrix is the
//        symmetry operator in rotation orthnormal matrix form.
//
//    Num_of_clusters : An integer which specify the number of clusters in
//        the samples.
//
//
//  Outputs:
//    Mu : A Num_of_clusters x 4 matrix which contains the estimated means of
//        each cluster in quaternion.
//
//    Kappa : A Num_of_clusters x 1 matrix which contains the estimated kappa
//        of each cluster.
//
//    W : A Num_of_clusters x 1 matrix which contains the mixing weight of
//        the clusters.
//
//    logL : The log-likelihood for the samples of the estimated model.
//
//    CIdx : The N x 1 matrix which indicates the cluster of each sample it
//        belongs to.
//
//  Function is written by Yu-Hui Chen, University of Michigan
// Arguments    : const emxArray_real_T *X
//                const emxArray_real_T *Pm
//                double Num_of_clusters
//                emxArray_real_T *Mu
//                emxArray_real_T *Kappa
//                emxArray_real_T *W
//                double *logL
//                emxArray_real_T *CIdx
// Return Type  : void
//
void EMmWatson(const emxArray_real_T *X, const emxArray_real_T *Pm, double
               Num_of_clusters, emxArray_real_T *Mu, emxArray_real_T *Kappa,
               emxArray_real_T *W, double *logL, emxArray_real_T *CIdx)
{
  emxArray_real_T *Mu_All;
  int N;
  int M;
  int i0;
  int br;
  emxArray_real_T *Kappa_All;
  emxArray_real_T *W_All;
  double L_All[5];
  int n;
  emxArray_real_T *R_All;
  int ixstart;
  int trial;
  emxArray_real_T *R;
  emxArray_real_T *Alpha;
  emxArray_real_T *Rdenom;
  emxArray_real_T *rX;
  emxArray_real_T *Phi;
  emxArray_real_T *a;
  emxArray_real_T *r0;
  emxArray_real_T *r1;
  emxArray_real_T *r2;
  emxArray_real_T *r3;
  emxArray_boolean_T *b_W;
  emxArray_real_T *b_R;
  emxArray_real_T *c_R;
  int ix;
  double mu[4];
  double mtmp;
  double Q[30];
  double L[30];
  boolean_T isfound;
  int ite;
  int itmp;
  boolean_T exitg3;
  int j;
  double b_Pm[4];
  double c_Pm;
  double dv0[3];
  int c_W[1];
  emxArray_real_T d_W;
  boolean_T exitg4;
  double T[16];
  double rXXT[16];
  int vstride;
  int cindx;
  int ixstop;
  int iy;
  double d_Pm[16];
  creal_T D[16];
  creal_T V[16];
  creal_T Mu_tmp[4];
  int iidx[4];
  int dd[4];
  creal_T b_T[16];
  creal_T b_V[4];
  double V_im;
  double Kappa_tmp;
  boolean_T guard1 = false;
  boolean_T exitg2;
  emxArray_real_T *b;
  unsigned int outsz[2];
  emxArray_int32_T *iindx;
  boolean_T exitg1;
  emxInit_real_T(&Mu_All, 3);

  // %% Duplicate the Euler Angles
  N = X->size[0];
  M = Pm->size[2];

  //  Precompute the xAp, yAp for invAp_fast
  i0 = Mu_All->size[0] * Mu_All->size[1] * Mu_All->size[2];
  Mu_All->size[0] = (int)Num_of_clusters;
  Mu_All->size[1] = 4;
  Mu_All->size[2] = 5;
  emxEnsureCapacity((emxArray__common *)Mu_All, i0, (int)sizeof(double));
  br = ((int)Num_of_clusters << 2) * 5;
  for (i0 = 0; i0 < br; i0++) {
    Mu_All->data[i0] = 0.0;
  }

  b_emxInit_real_T(&Kappa_All, 2);
  i0 = Kappa_All->size[0] * Kappa_All->size[1];
  Kappa_All->size[0] = (int)Num_of_clusters;
  Kappa_All->size[1] = 5;
  emxEnsureCapacity((emxArray__common *)Kappa_All, i0, (int)sizeof(double));
  br = (int)Num_of_clusters * 5;
  for (i0 = 0; i0 < br; i0++) {
    Kappa_All->data[i0] = 0.0;
  }

  b_emxInit_real_T(&W_All, 2);
  i0 = W_All->size[0] * W_All->size[1];
  W_All->size[0] = (int)Num_of_clusters;
  W_All->size[1] = 5;
  emxEnsureCapacity((emxArray__common *)W_All, i0, (int)sizeof(double));
  br = (int)Num_of_clusters * 5;
  for (i0 = 0; i0 < br; i0++) {
    W_All->data[i0] = 0.0;
  }

  for (n = 0; n < 5; n++) {
    L_All[n] = 0.0;
  }

  emxInit_real_T(&R_All, 3);
  ixstart = X->size[0];
  i0 = R_All->size[0] * R_All->size[1] * R_All->size[2];
  R_All->size[0] = ixstart;
  emxEnsureCapacity((emxArray__common *)R_All, i0, (int)sizeof(double));
  i0 = R_All->size[0] * R_All->size[1] * R_All->size[2];
  R_All->size[1] = (int)Num_of_clusters;
  R_All->size[2] = 5;
  emxEnsureCapacity((emxArray__common *)R_All, i0, (int)sizeof(double));
  br = X->size[0] * (int)Num_of_clusters * 5;
  for (i0 = 0; i0 < br; i0++) {
    R_All->data[i0] = 0.0;
  }

  trial = 0;
  emxInit_real_T(&R, 3);
  c_emxInit_real_T(&Alpha, 1);
  c_emxInit_real_T(&Rdenom, 1);
  b_emxInit_real_T(&rX, 2);
  emxInit_real_T(&Phi, 3);
  b_emxInit_real_T(&a, 2);
  b_emxInit_real_T(&r0, 2);
  emxInit_real_T(&r1, 3);
  emxInit_real_T(&r2, 3);
  emxInit_real_T(&r3, 3);
  emxInit_boolean_T(&b_W, 1);
  emxInit_real_T(&b_R, 3);
  emxInit_real_T(&c_R, 3);
  while (trial + 1 <= 5) {
    //  Initialize Mu and Kappa
    i0 = R->size[0] * R->size[1] * R->size[2];
    R->size[0] = N;
    R->size[1] = M;
    R->size[2] = (int)Num_of_clusters;
    emxEnsureCapacity((emxArray__common *)R, i0, (int)sizeof(double));
    br = N * M * (int)Num_of_clusters;
    for (i0 = 0; i0 < br; i0++) {
      R->data[i0] = 0.0;
    }

    i0 = Mu->size[0] * Mu->size[1];
    Mu->size[0] = (int)Num_of_clusters;
    Mu->size[1] = 4;
    emxEnsureCapacity((emxArray__common *)Mu, i0, (int)sizeof(double));
    br = (int)Num_of_clusters << 2;
    for (i0 = 0; i0 < br; i0++) {
      Mu->data[i0] = 0.0;
    }

    for (ix = 0; ix < (int)Num_of_clusters; ix++) {
      randn(mu);
      mtmp = norm(mu);
      for (i0 = 0; i0 < 4; i0++) {
        mu[i0] /= mtmp;
      }

      for (i0 = 0; i0 < 4; i0++) {
        Mu->data[ix + Mu->size[0] * i0] = mu[i0];
      }
    }

    i0 = Kappa->size[0];
    Kappa->size[0] = (int)Num_of_clusters;
    emxEnsureCapacity((emxArray__common *)Kappa, i0, (int)sizeof(double));
    br = (int)Num_of_clusters;
    for (i0 = 0; i0 < br; i0++) {
      Kappa->data[i0] = 100.0;
    }

    mtmp = 1.0 / Num_of_clusters;
    i0 = W->size[0];
    W->size[0] = (int)Num_of_clusters;
    emxEnsureCapacity((emxArray__common *)W, i0, (int)sizeof(double));
    br = (int)Num_of_clusters;
    for (i0 = 0; i0 < br; i0++) {
      W->data[i0] = mtmp;
    }

    mtmp = 1.0 / (double)M;
    i0 = Alpha->size[0];
    Alpha->size[0] = M;
    emxEnsureCapacity((emxArray__common *)Alpha, i0, (int)sizeof(double));
    for (i0 = 0; i0 < M; i0++) {
      Alpha->data[i0] = mtmp;
    }

    for (n = 0; n < 30; n++) {
      Q[n] = 0.0;
      L[n] = 0.0;
    }

    isfound = true;
    ite = 0;
    itmp = 0;
    exitg3 = false;
    while ((!exitg3) && (itmp < 30)) {
      ite = itmp;

      // %% E-step
      for (ix = 0; ix < (int)Num_of_clusters; ix++) {
        for (j = 0; j < M; j++) {
          mtmp = W->data[ix] * Alpha->data[j];
          for (i0 = 0; i0 < 4; i0++) {
            b_Pm[i0] = 0.0;
            for (n = 0; n < 4; n++) {
              c_Pm = b_Pm[i0] + Pm->data[(i0 + Pm->size[0] * n) + Pm->size[0] *
                Pm->size[1] * j] * Mu->data[ix + Mu->size[0] * n];
              b_Pm[i0] = c_Pm;
            }
          }

          WatsonDensity(X, b_Pm, Kappa->data[ix], Rdenom);
          br = Rdenom->size[0];
          for (i0 = 0; i0 < br; i0++) {
            R->data[(i0 + R->size[0] * j) + R->size[0] * R->size[1] * ix] = mtmp
              * Rdenom->data[i0] + 2.2204460492503131E-16;
          }
        }
      }

      //  Normalization
      sum(R, r0);
      b_sum(r0, Rdenom);
      dv0[0] = 1.0;
      dv0[1] = M;
      dv0[2] = Num_of_clusters;
      repmat(Rdenom, dv0, Phi);
      i0 = b_R->size[0] * b_R->size[1] * b_R->size[2];
      b_R->size[0] = R->size[0];
      b_R->size[1] = R->size[1];
      b_R->size[2] = R->size[2];
      emxEnsureCapacity((emxArray__common *)b_R, i0, (int)sizeof(double));
      br = R->size[0] * R->size[1] * R->size[2];
      for (i0 = 0; i0 < br; i0++) {
        b_R->data[i0] = R->data[i0];
      }

      rdivide(b_R, Phi, R);

      //  M-step
      //  estimate W
      c_sum(R, r2);
      d_sum(r2, r3);
      squeeze(r3, W);
      c_W[0] = W->size[0];
      d_W = *W;
      d_W.size = (int *)&c_W;
      d_W.numDimensions = 1;
      mtmp = e_sum(&d_W);
      i0 = W->size[0];
      emxEnsureCapacity((emxArray__common *)W, i0, (int)sizeof(double));
      br = W->size[0];
      for (i0 = 0; i0 < br; i0++) {
        W->data[i0] /= mtmp;
      }

      i0 = b_W->size[0];
      b_W->size[0] = W->size[0];
      emxEnsureCapacity((emxArray__common *)b_W, i0, (int)sizeof(boolean_T));
      br = W->size[0];
      for (i0 = 0; i0 < br; i0++) {
        b_W->data[i0] = (W->data[i0] < 0.001);
      }

      if (any(b_W)) {
        isfound = false;
        exitg3 = true;
      } else {
        // %% Estimate mu
        ix = 0;
        exitg4 = false;
        while ((!exitg4) && (ix <= (int)Num_of_clusters - 1)) {
          //  Calculate Scatter Matrix T
          memset(&T[0], 0, sizeof(double) << 4);
          for (j = 0; j < M; j++) {
            br = R->size[0];
            i0 = Rdenom->size[0];
            Rdenom->size[0] = br;
            emxEnsureCapacity((emxArray__common *)Rdenom, i0, (int)sizeof(double));
            for (i0 = 0; i0 < br; i0++) {
              Rdenom->data[i0] = R->data[(i0 + R->size[0] * j) + R->size[0] *
                R->size[1] * ix];
            }

            b_sqrt(Rdenom);
            b_repmat(Rdenom, rX);
            i0 = rX->size[0] * rX->size[1];
            rX->size[1] = 4;
            emxEnsureCapacity((emxArray__common *)rX, i0, (int)sizeof(double));
            ixstart = rX->size[0];
            br = rX->size[1];
            br *= ixstart;
            for (i0 = 0; i0 < br; i0++) {
              rX->data[i0] *= X->data[i0];
            }

            i0 = a->size[0] * a->size[1];
            a->size[0] = 4;
            a->size[1] = rX->size[0];
            emxEnsureCapacity((emxArray__common *)a, i0, (int)sizeof(double));
            br = rX->size[0];
            for (i0 = 0; i0 < br; i0++) {
              for (n = 0; n < 4; n++) {
                a->data[n + a->size[0] * i0] = rX->data[i0 + rX->size[0] * n];
              }
            }

            if ((a->size[1] == 1) || (rX->size[0] == 1)) {
              for (i0 = 0; i0 < 4; i0++) {
                for (n = 0; n < 4; n++) {
                  rXXT[i0 + (n << 2)] = 0.0;
                  br = a->size[1];
                  for (vstride = 0; vstride < br; vstride++) {
                    rXXT[i0 + (n << 2)] += a->data[i0 + a->size[0] * vstride] *
                      rX->data[vstride + rX->size[0] * n];
                  }
                }
              }
            } else {
              cindx = a->size[1];
              memset(&rXXT[0], 0, sizeof(double) << 4);
              for (ixstart = 0; ixstart < 14; ixstart += 4) {
                for (ixstop = ixstart; ixstop + 1 <= ixstart + 4; ixstop++) {
                  rXXT[ixstop] = 0.0;
                }
              }

              br = 0;
              for (ixstart = 0; ixstart < 14; ixstart += 4) {
                n = 0;
                i0 = br + cindx;
                for (vstride = br; vstride + 1 <= i0; vstride++) {
                  if (rX->data[vstride] != 0.0) {
                    iy = n;
                    for (ixstop = ixstart; ixstop + 1 <= ixstart + 4; ixstop++)
                    {
                      iy++;
                      rXXT[ixstop] += rX->data[vstride] * a->data[iy - 1];
                    }
                  }

                  n += 4;
                }

                br += cindx;
              }
            }

            for (i0 = 0; i0 < 4; i0++) {
              for (n = 0; n < 4; n++) {
                d_Pm[i0 + (n << 2)] = 0.0;
                for (vstride = 0; vstride < 4; vstride++) {
                  d_Pm[i0 + (n << 2)] += Pm->data[(vstride + Pm->size[0] * i0) +
                    Pm->size[0] * Pm->size[1] * j] * rXXT[vstride + (n << 2)];
                }
              }
            }

            for (i0 = 0; i0 < 4; i0++) {
              for (n = 0; n < 4; n++) {
                mtmp = 0.0;
                for (vstride = 0; vstride < 4; vstride++) {
                  mtmp += d_Pm[i0 + (vstride << 2)] * Pm->data[(vstride +
                    Pm->size[0] * n) + Pm->size[0] * Pm->size[1] * j];
                }

                T[i0 + (n << 2)] += mtmp;
              }
            }
          }

          mtmp = (double)N * W->data[ix];
          for (i0 = 0; i0 < 16; i0++) {
            T[i0] /= mtmp;
          }

          eig(T, V, D);
          diag(D, Mu_tmp);
          eml_sort(Mu_tmp, iidx);

          // %% Estimate Kappa
          for (n = 0; n < 4; n++) {
            dd[n] = iidx[n];
            for (i0 = 0; i0 < 4; i0++) {
              b_T[i0 + (n << 2)].re = T[i0 + (n << 2)];
              b_T[i0 + (n << 2)].im = 0.0;
            }
          }

          for (i0 = 0; i0 < 4; i0++) {
            b_V[i0].re = 0.0;
            b_V[i0].im = 0.0;
            for (n = 0; n < 4; n++) {
              mtmp = V[n + ((dd[0] - 1) << 2)].re;
              V_im = -V[n + ((dd[0] - 1) << 2)].im;
              b_V[i0].re += mtmp * b_T[n + (i0 << 2)].re - V_im * 0.0;
              b_V[i0].im += mtmp * 0.0 + V_im * b_T[n + (i0 << 2)].re;
            }
          }

          mtmp = 0.0;
          for (i0 = 0; i0 < 4; i0++) {
            mtmp += b_V[i0].re * V[i0 + ((dd[0] - 1) << 2)].re - b_V[i0].im *
              V[i0 + ((dd[0] - 1) << 2)].im;
          }

          Kappa_tmp = invDp(mtmp);
          memcpy(&Mu_tmp[0], &V[(dd[0] - 1) << 2], sizeof(creal_T) << 2);
          guard1 = false;
          if (Kappa_tmp < 0.0) {
            // %% The other case, need to use Mu_p
            for (i0 = 0; i0 < 4; i0++) {
              for (n = 0; n < 4; n++) {
                b_T[n + (i0 << 2)].re = T[n + (i0 << 2)];
                b_T[n + (i0 << 2)].im = 0.0;
              }

              b_V[i0].re = 0.0;
              b_V[i0].im = 0.0;
              for (n = 0; n < 4; n++) {
                mtmp = V[n + ((dd[3] - 1) << 2)].re;
                V_im = -V[n + ((dd[3] - 1) << 2)].im;
                b_V[i0].re += mtmp * b_T[n + (i0 << 2)].re - V_im * 0.0;
                b_V[i0].im += mtmp * 0.0 + V_im * b_T[n + (i0 << 2)].re;
              }
            }

            mtmp = 0.0;
            for (i0 = 0; i0 < 4; i0++) {
              mtmp += b_V[i0].re * V[i0 + ((dd[3] - 1) << 2)].re - b_V[i0].im *
                V[i0 + ((dd[3] - 1) << 2)].im;
            }

            Kappa_tmp = invDp(mtmp);
            memcpy(&Mu_tmp[0], &V[(dd[3] - 1) << 2], sizeof(creal_T) << 2);
            if (Kappa_tmp > 0.0) {
              i0 = Kappa->size[0];
              Kappa->size[0] = (int)Num_of_clusters;
              emxEnsureCapacity((emxArray__common *)Kappa, i0, (int)sizeof
                                (double));
              br = (int)Num_of_clusters;
              for (i0 = 0; i0 < br; i0++) {
                Kappa->data[i0] = 0.0;
              }

              i0 = Mu->size[0] * Mu->size[1];
              Mu->size[0] = (int)Num_of_clusters;
              Mu->size[1] = 4;
              emxEnsureCapacity((emxArray__common *)Mu, i0, (int)sizeof(double));
              br = (int)Num_of_clusters << 2;
              for (i0 = 0; i0 < br; i0++) {
                Mu->data[i0] = 0.0;
              }

              isfound = false;
              exitg4 = true;
            } else {
              guard1 = true;
            }
          } else {
            guard1 = true;
          }

          if (guard1) {
            for (i0 = 0; i0 < 4; i0++) {
              Mu->data[ix + Mu->size[0] * i0] = Mu_tmp[i0].re;
            }

            Kappa->data[ix] = Kappa_tmp;
            ix++;
          }
        }

        if (!isfound) {
          exitg3 = true;
        } else {
          ixstart = Kappa->size[0];
          for (n = 0; n < ixstart; n++) {
            if (Kappa->data[n] > 710.0) {
              Kappa->data[n] = 710.0;
            }
          }

          //  Calculate the Q function
          i0 = Phi->size[0] * Phi->size[1] * Phi->size[2];
          Phi->size[0] = N;
          Phi->size[1] = M;
          Phi->size[2] = (int)Num_of_clusters;
          emxEnsureCapacity((emxArray__common *)Phi, i0, (int)sizeof(double));
          br = N * M * (int)Num_of_clusters;
          for (i0 = 0; i0 < br; i0++) {
            Phi->data[i0] = 0.0;
          }

          for (ix = 0; ix < (int)Num_of_clusters; ix++) {
            for (j = 0; j < M; j++) {
              mtmp = W->data[ix] * Alpha->data[j];
              for (i0 = 0; i0 < 4; i0++) {
                b_Pm[i0] = 0.0;
                for (n = 0; n < 4; n++) {
                  c_Pm = b_Pm[i0] + Pm->data[(i0 + Pm->size[0] * n) + Pm->size[0]
                    * Pm->size[1] * j] * Mu->data[ix + Mu->size[0] * n];
                  b_Pm[i0] = c_Pm;
                }
              }

              WatsonDensity(X, b_Pm, Kappa->data[ix], Rdenom);
              br = Rdenom->size[0];
              for (i0 = 0; i0 < br; i0++) {
                Phi->data[(i0 + Phi->size[0] * j) + Phi->size[0] * Phi->size[1] *
                  ix] = mtmp * Rdenom->data[i0];
              }
            }
          }

          sum(Phi, r0);
          b_sum(r0, Rdenom);
          b_log(Rdenom);
          L[itmp] = e_sum(Rdenom);
          i0 = Phi->size[0] * Phi->size[1] * Phi->size[2];
          emxEnsureCapacity((emxArray__common *)Phi, i0, (int)sizeof(double));
          ixstart = Phi->size[0];
          n = Phi->size[1];
          br = Phi->size[2];
          br *= ixstart * n;
          for (i0 = 0; i0 < br; i0++) {
            Phi->data[i0] += 2.2204460492503131E-16;
          }

          c_log(Phi);
          i0 = c_R->size[0] * c_R->size[1] * c_R->size[2];
          c_R->size[0] = R->size[0];
          c_R->size[1] = R->size[1];
          c_R->size[2] = R->size[2];
          emxEnsureCapacity((emxArray__common *)c_R, i0, (int)sizeof(double));
          br = R->size[0] * R->size[1] * R->size[2];
          for (i0 = 0; i0 < br; i0++) {
            c_R->data[i0] = R->data[i0] * Phi->data[i0];
          }

          f_sum(c_R, r2);
          g_sum(r2, r3);
          Q[itmp] = h_sum(r3);
          if ((1 + itmp >= 2) && (fabs(Q[itmp] - Q[itmp - 1]) < 0.05)) {
            exitg3 = true;
          } else {
            itmp++;
          }
        }
      }
    }

    if (isfound) {
      //  update the containers
      for (i0 = 0; i0 < 4; i0++) {
        br = Mu->size[0];
        for (n = 0; n < br; n++) {
          Mu_All->data[(n + Mu_All->size[0] * i0) + Mu_All->size[0] *
            Mu_All->size[1] * trial] = Mu->data[n + Mu->size[0] * i0];
        }
      }

      ixstart = Kappa->size[0];
      for (i0 = 0; i0 < ixstart; i0++) {
        Kappa_All->data[i0 + Kappa_All->size[0] * trial] = Kappa->data[i0];
      }

      ixstart = W->size[0];
      for (i0 = 0; i0 < ixstart; i0++) {
        W_All->data[i0 + W_All->size[0] * trial] = W->data[i0];
      }

      L_All[trial] = L[ite];
      i_sum(R, r1);
      b_squeeze(r1, r0);
      br = r0->size[1];
      for (i0 = 0; i0 < br; i0++) {
        ixstart = r0->size[0];
        for (n = 0; n < ixstart; n++) {
          R_All->data[(n + R_All->size[0] * i0) + R_All->size[0] * R_All->size[1]
            * trial] = r0->data[n + r0->size[0] * i0];
        }
      }

      trial++;
    }
  }

  emxFree_real_T(&c_R);
  emxFree_real_T(&b_R);
  emxFree_boolean_T(&b_W);
  emxFree_real_T(&r3);
  emxFree_real_T(&r2);
  emxFree_real_T(&r1);
  emxFree_real_T(&a);
  emxFree_real_T(&Phi);
  emxFree_real_T(&rX);
  emxFree_real_T(&Rdenom);
  emxFree_real_T(&Alpha);
  emxFree_real_T(&R);
  ixstart = 1;
  mtmp = L_All[0];
  itmp = 0;
  if (rtIsNaN(L_All[0])) {
    br = 1;
    exitg2 = false;
    while ((!exitg2) && (br + 1 < 6)) {
      ixstart = br + 1;
      if (!rtIsNaN(L_All[br])) {
        mtmp = L_All[br];
        itmp = br;
        exitg2 = true;
      } else {
        br++;
      }
    }
  }

  if (ixstart < 5) {
    while (ixstart + 1 < 6) {
      if (L_All[ixstart] > mtmp) {
        mtmp = L_All[ixstart];
        itmp = ixstart;
      }

      ixstart++;
    }
  }

  br = Mu_All->size[0];
  i0 = Mu->size[0] * Mu->size[1];
  Mu->size[0] = br;
  Mu->size[1] = 4;
  emxEnsureCapacity((emxArray__common *)Mu, i0, (int)sizeof(double));
  for (i0 = 0; i0 < 4; i0++) {
    for (n = 0; n < br; n++) {
      Mu->data[n + Mu->size[0] * i0] = Mu_All->data[(n + Mu_All->size[0] * i0) +
        Mu_All->size[0] * Mu_All->size[1] * itmp];
    }
  }

  emxFree_real_T(&Mu_All);
  br = Kappa_All->size[0];
  i0 = Kappa->size[0];
  Kappa->size[0] = br;
  emxEnsureCapacity((emxArray__common *)Kappa, i0, (int)sizeof(double));
  for (i0 = 0; i0 < br; i0++) {
    Kappa->data[i0] = Kappa_All->data[i0 + Kappa_All->size[0] * itmp];
  }

  emxFree_real_T(&Kappa_All);
  br = W_All->size[0];
  i0 = W->size[0];
  W->size[0] = br;
  emxEnsureCapacity((emxArray__common *)W, i0, (int)sizeof(double));
  for (i0 = 0; i0 < br; i0++) {
    W->data[i0] = W_All->data[i0 + W_All->size[0] * itmp];
  }

  emxFree_real_T(&W_All);
  *logL = L_All[itmp];
  if (Num_of_clusters > 1.0) {
    b_emxInit_real_T(&b, 2);
    i0 = R_All->size[0];
    n = R_All->size[1];
    vstride = b->size[0] * b->size[1];
    b->size[0] = i0;
    b->size[1] = n;
    emxEnsureCapacity((emxArray__common *)b, vstride, (int)sizeof(double));
    i0 = R_All->size[0];
    n = R_All->size[1];
    i0 *= n;
    for (cindx = 0; cindx + 1 <= i0; cindx++) {
      br = R_All->size[0];
      ixstart = R_All->size[1];
      n = r0->size[0] * r0->size[1];
      r0->size[0] = br;
      r0->size[1] = ixstart;
      emxEnsureCapacity((emxArray__common *)r0, n, (int)sizeof(double));
      for (n = 0; n < ixstart; n++) {
        for (vstride = 0; vstride < br; vstride++) {
          r0->data[vstride + r0->size[0] * n] = R_All->data[(vstride +
            R_All->size[0] * n) + R_All->size[0] * R_All->size[1] * itmp];
        }
      }

      n = r0->size[0];
      b->data[cindx] = r0->data[cindx % r0->size[0] + r0->size[0] *
        div_nzp_s32_floor(cindx, n)];
    }

    for (i0 = 0; i0 < 2; i0++) {
      outsz[i0] = (unsigned int)b->size[i0];
    }

    emxInit_int32_T(&iindx, 1);
    i0 = iindx->size[0];
    iindx->size[0] = (int)outsz[0];
    emxEnsureCapacity((emxArray__common *)iindx, i0, (int)sizeof(int));
    br = (int)outsz[0];
    for (i0 = 0; i0 < br; i0++) {
      iindx->data[i0] = 1;
    }

    n = b->size[1];
    vstride = b->size[0];
    br = 0;
    iy = -1;
    for (j = 1; j <= vstride; j++) {
      br++;
      ixstart = br;
      ixstop = br + (n - 1) * vstride;
      mtmp = b->data[br - 1];
      itmp = 1;
      if (n > 1) {
        cindx = 1;
        if (rtIsNaN(b->data[br - 1])) {
          ix = br + vstride;
          exitg1 = false;
          while ((!exitg1) && ((vstride > 0) && (ix <= ixstop))) {
            cindx++;
            ixstart = ix;
            if (!rtIsNaN(b->data[ix - 1])) {
              mtmp = b->data[ix - 1];
              itmp = cindx;
              exitg1 = true;
            } else {
              ix += vstride;
            }
          }
        }

        if (ixstart < ixstop) {
          ix = ixstart + vstride;
          while ((vstride > 0) && (ix <= ixstop)) {
            cindx++;
            if (b->data[ix - 1] > mtmp) {
              mtmp = b->data[ix - 1];
              itmp = cindx;
            }

            ix += vstride;
          }
        }
      }

      iy++;
      iindx->data[iy] = itmp;
    }

    emxFree_real_T(&b);
    i0 = CIdx->size[0];
    CIdx->size[0] = iindx->size[0];
    emxEnsureCapacity((emxArray__common *)CIdx, i0, (int)sizeof(double));
    br = iindx->size[0];
    for (i0 = 0; i0 < br; i0++) {
      CIdx->data[i0] = iindx->data[i0];
    }

    emxFree_int32_T(&iindx);
  } else {
    i0 = CIdx->size[0];
    CIdx->size[0] = X->size[0];
    emxEnsureCapacity((emxArray__common *)CIdx, i0, (int)sizeof(double));
    br = X->size[0];
    for (i0 = 0; i0 < br; i0++) {
      CIdx->data[i0] = 1.0;
    }
  }

  emxFree_real_T(&r0);
  emxFree_real_T(&R_All);
}

//
// File trailer for EMmWatson.cpp
//
// [EOF]
//
