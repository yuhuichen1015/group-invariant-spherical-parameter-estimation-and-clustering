//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: WatsonDensity.h
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 04-Aug-2015 13:05:25
//
#ifndef __WATSONDENSITY_H__
#define __WATSONDENSITY_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "EMmWatson_types.h"

// Function Declarations
extern void WatsonDensity(const emxArray_real_T *x, const double mu[4], double k,
  emxArray_real_T *y);

#endif

//
// File trailer for WatsonDensity.h
//
// [EOF]
//
