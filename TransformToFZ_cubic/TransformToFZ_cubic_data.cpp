//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: TransformToFZ_cubic_data.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 28-Jul-2015 15:20:12
//

// Include Files
#include "rt_nonfinite.h"
#include "TransformToFZ_cubic.h"
#include "TransformToFZ_cubic_data.h"

// Variable Definitions
double Pm[384];

//
// File trailer for TransformToFZ_cubic_data.cpp
//
// [EOF]
//
