//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: TransformToFZ_cubic_terminate.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 28-Jul-2015 15:35:39
//

// Include Files
#include "rt_nonfinite.h"
#include "TransformToFZ_cubic.h"
#include "TransformToFZ_cubic_terminate.h"

// Function Definitions

//
// Arguments    : void
// Return Type  : void
//
void TransformToFZ_cubic_terminate()
{
  // (no terminate code required)
}

//
// File trailer for TransformToFZ_cubic_terminate.cpp
//
// [EOF]
//
