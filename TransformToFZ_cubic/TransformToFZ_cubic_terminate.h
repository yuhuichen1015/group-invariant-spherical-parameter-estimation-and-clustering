//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: TransformToFZ_cubic_terminate.h
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 28-Jul-2015 15:35:39
//
#ifndef __TRANSFORMTOFZ_CUBIC_TERMINATE_H__
#define __TRANSFORMTOFZ_CUBIC_TERMINATE_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rtwtypes.h"
#include "TransformToFZ_cubic_types.h"

// Function Declarations
extern void TransformToFZ_cubic_terminate();

#endif

//
// File trailer for TransformToFZ_cubic_terminate.h
//
// [EOF]
//
