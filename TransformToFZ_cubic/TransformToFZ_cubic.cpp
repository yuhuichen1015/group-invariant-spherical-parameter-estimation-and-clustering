//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: TransformToFZ_cubic.cpp
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 28-Jul-2015 15:35:39
//

// Include Files
#include "rt_nonfinite.h"
#include "TransformToFZ_cubic.h"
#include "TransformToFZ_cubic_emxutil.h"

// Function Definitions

//
// This function convert the quaternions into pre-defined fundamental zone
//  for cubic symmetry.
//
//  Usage:
//   [QuatFZ] = Quat2CubicFZ(Quat)
//
//  inputs,
//    Quat : A N x 4 matrix which contains the N quaternions that need to be
//            converted into fundamental zone.
//
//
//  outputs,
//    QuatFZ : A N x 4 matrix which contains the N quaternions that are in the
//            fundamental zone.
//
//  Function is written by Yu-Hui Chen, University of Michigan
// Arguments    : const emxArray_real_T *Quat
//                emxArray_real_T *QuatFZ
// Return Type  : void
//
void TransformToFZ_cubic(const emxArray_real_T *Quat, emxArray_real_T *QuatFZ)
{
  unsigned int Sz[2];
  int i0;
  int k;
  int s;
  double q[4];
  double repRod[72];
  int i1;
  static const double dv0[384] = { 1.0, -0.0, 0.0, -0.0, 0.0, 1.0, -0.0, -0.0,
    -0.0, 0.0, 1.0, -0.0, 0.0, 0.0, 0.0, 1.0, 0.0, -0.0, 0.0, -1.0, 0.0, 0.0,
    -1.0, -0.0, -0.0, 1.0, 0.0, -0.0, 1.0, 0.0, 0.0, 0.0, 0.0, -0.0, 1.0, -0.0,
    0.0, 0.0, -0.0, -1.0, -1.0, 0.0, 0.0, -0.0, 0.0, 1.0, 0.0, 0.0, 0.0, -1.0,
    0.0, -0.0, 1.0, 0.0, -0.0, -0.0, -0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 1.0, 0.0,
    0.707106781, -0.0, 0.0, -0.707106781, 0.0, 0.707106781, -0.707106781, -0.0,
    -0.0, 0.707106781, 0.707106781, -0.0, 0.707106781, 0.0, 0.0, 0.707106781,
    0.707106781, -0.0, 0.707106781, -0.0, 0.0, 0.707106781, -0.0, -0.707106781,
    -0.707106781, 0.0, 0.707106781, -0.0, 0.0, 0.707106781, 0.0, 0.707106781,
    0.707106781, -0.707106781, 0.0, -0.0, 0.707106781, 0.707106781, -0.0, -0.0,
    -0.0, 0.0, 0.707106781, -0.707106781, 0.0, 0.0, 0.707106781, 0.707106781,
    0.707106781, -0.0, 0.0, 0.707106781, 0.0, 0.707106781, 0.707106781, -0.0,
    -0.0, -0.707106781, 0.707106781, -0.0, -0.707106781, 0.0, 0.0, 0.707106781,
    0.707106781, -0.0, -0.707106781, -0.0, 0.0, 0.707106781, -0.0, 0.707106781,
    0.707106781, 0.0, 0.707106781, -0.0, 0.0, -0.707106781, 0.0, 0.707106781,
    0.707106781, 0.707106781, 0.0, -0.0, -0.707106781, 0.707106781, -0.0, -0.0,
    -0.0, 0.0, 0.707106781, 0.707106781, 0.0, 0.0, -0.707106781, 0.707106781,
    0.0, -0.0, 0.707106781, -0.707106781, 0.0, 0.0, -0.707106781, -0.707106781,
    -0.707106781, 0.707106781, 0.0, -0.0, 0.707106781, 0.707106781, 0.0, 0.0,
    0.0, -0.0, 0.707106781, 0.707106781, 0.0, 0.0, 0.707106781, -0.707106781,
    -0.707106781, -0.707106781, 0.0, -0.0, -0.707106781, 0.707106781, 0.0, 0.0,
    0.0, -0.707106781, 0.707106781, -0.0, 0.707106781, 0.0, -0.0, -0.707106781,
    -0.707106781, 0.0, 0.0, -0.707106781, 0.0, 0.707106781, 0.707106781, 0.0,
    0.0, -0.707106781, -0.707106781, -0.0, 0.707106781, 0.0, -0.0, 0.707106781,
    0.707106781, 0.0, 0.0, -0.707106781, 0.0, -0.707106781, 0.707106781, 0.0,
    0.0, -0.707106781, 0.0, -0.707106781, 0.707106781, 0.0, -0.707106781, -0.0,
    -0.0, 0.707106781, 0.0, -0.707106781, 0.707106781, 0.0, 0.707106781, 0.0,
    0.0, -0.707106781, 0.0, 0.707106781, 0.707106781, 0.0, 0.707106781, -0.0,
    -0.0, -0.707106781, 0.0, -0.707106781, -0.707106781, 0.0, 0.707106781, 0.0,
    0.5, -0.5, 0.5, -0.5, 0.5, 0.5, -0.5, -0.5, -0.5, 0.5, 0.5, -0.5, 0.5, 0.5,
    0.5, 0.5, 0.5, 0.5, -0.5, 0.5, -0.5, 0.5, 0.5, 0.5, 0.5, -0.5, 0.5, 0.5,
    -0.5, -0.5, -0.5, 0.5, 0.5, -0.5, -0.5, -0.5, 0.5, 0.5, -0.5, 0.5, 0.5, 0.5,
    0.5, -0.5, 0.5, -0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, -0.5, 0.5, 0.5, -0.5,
    -0.5, -0.5, 0.5, 0.5, -0.5, 0.5, -0.5, 0.5, 0.5, -0.5, 0.5, 0.5, 0.5, 0.5,
    0.5, -0.5, -0.5, -0.5, 0.5, -0.5, -0.5, 0.5, 0.5, 0.5, 0.5, 0.5, -0.5, -0.5,
    -0.5, 0.5, -0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, -0.5, -0.5, 0.5, 0.5, -0.5,
    -0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, -0.5, 0.5, -0.5, -0.5, -0.5, 0.5, 0.5,
    0.5, 0.5, 0.5, -0.5, -0.5, 0.5, -0.5, -0.5, -0.5, 0.5, 0.5, 0.5, 0.5, 0.5,
    -0.5, 0.5 };

  double b_q;
  int idx;
  boolean_T exitg1;
  double y;
  double scale;
  double absxk;
  double t;
  double normR2;

  // %% Start to convert the quaternions into fundamental zone
  for (i0 = 0; i0 < 2; i0++) {
    Sz[i0] = (unsigned int)Quat->size[i0];
  }

  i0 = QuatFZ->size[0] * QuatFZ->size[1];
  QuatFZ->size[0] = (int)Sz[0];
  QuatFZ->size[1] = 4;
  emxEnsureCapacity((emxArray__common *)QuatFZ, i0, (int)sizeof(double));
  k = (int)Sz[0] << 2;
  for (i0 = 0; i0 < k; i0++) {
    QuatFZ->data[i0] = 0.0;
  }

  for (s = 0; s < (int)Sz[0]; s++) {
    //  Loop through each input quaternion
    for (k = 0; k < 24; k++) {
      //  replicate the quaternion by the 24 operators
      for (i0 = 0; i0 < 4; i0++) {
        q[i0] = 0.0;
        for (i1 = 0; i1 < 4; i1++) {
          b_q = q[i0] + dv0[(i0 + (i1 << 2)) + (k << 4)] * Quat->data[s +
            Quat->size[0] * i1];
          q[i0] = b_q;
        }
      }

      repRod[3 * k] = q[0] / q[3];
      repRod[1 + 3 * k] = q[1] / q[3];
      repRod[2 + 3 * k] = q[2] / q[3];
    }

    idx = 0;
    k = 0;
    exitg1 = false;
    while ((!exitg1) && (k < 24)) {
      idx = k;

      //  Loop through each replicates to check which one is in FZ
      //  The inequalities which defined the FZ
      if ((fabs(repRod[3 * k]) <= 0.41421356237309515) && (fabs(repRod[1 + 3 * k])
           <= 0.41421356237309515) && (fabs(repRod[2 + 3 * k]) <=
           0.41421356237309515) && (fabs((repRod[3 * k] + repRod[1 + 3 * k]) +
            repRod[2 + 3 * k]) <= 1.0) && (fabs((repRod[3 * k] - repRod[1 + 3 *
             k]) + repRod[2 + 3 * k]) <= 1.0) && (fabs((repRod[3 * k] + repRod[1
             + 3 * k]) - repRod[2 + 3 * k]) <= 1.0) && (fabs((-repRod[3 * k] +
             repRod[1 + 3 * k]) + repRod[2 + 3 * k]) <= 1.0)) {
        exitg1 = true;
      } else {
        k++;
      }
    }

    //  Convert Rodrigues vec back to quaternion
    y = 0.0;
    scale = 2.2250738585072014E-308;
    for (k = 0; k < 3; k++) {
      absxk = fabs(repRod[k + 3 * idx]);
      if (absxk > scale) {
        t = scale / absxk;
        y = 1.0 + y * t * t;
        scale = absxk;
      } else {
        t = absxk / scale;
        y += t * t;
      }
    }

    y = scale * sqrt(y);
    normR2 = y * y;
    q[3] = 1.0 / sqrt(1.0 + normR2);
    q[0] = repRod[3 * idx] / sqrt(1.0 + normR2);
    q[1] = repRod[1 + 3 * idx] / sqrt(1.0 + normR2);
    q[2] = repRod[2 + 3 * idx] / sqrt(1.0 + normR2);
    for (i0 = 0; i0 < 4; i0++) {
      QuatFZ->data[s + QuatFZ->size[0] * i0] = q[i0];
    }
  }
}

//
// File trailer for TransformToFZ_cubic.cpp
//
// [EOF]
//
