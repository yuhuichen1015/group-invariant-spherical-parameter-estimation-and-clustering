//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: TransformToFZ_cubic_data.h
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 28-Jul-2015 15:20:12
//
#ifndef __TRANSFORMTOFZ_CUBIC_DATA_H__
#define __TRANSFORMTOFZ_CUBIC_DATA_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rtwtypes.h"
#include "TransformToFZ_cubic_types.h"

// Variable Declarations
extern double Pm[384];

#endif

//
// File trailer for TransformToFZ_cubic_data.h
//
// [EOF]
//
