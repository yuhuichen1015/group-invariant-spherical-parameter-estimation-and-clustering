# README #

The project contains the C++ codes for the group-invariant spherical parameter estimation and clustering work. For the detail of the algorithm, please refer to our papers:

Yu-Hui Chen, Dennis Wei, Gregory Newstadt, Marc DeGraef, Jeffrey Simmons, Alfred Hero "[Parameter estimation in spherical symmetry groups.](http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=7001052)" *Signal Processing Letters, IEEE* 22.8 (2015): 1152-1155.

Yu-Hui Chen, Dennis Wei, Gregory Newstadt, Marc DeGraef, Jeffrey Simmons, Alfred Hero "[Statistical Estimation and Clustering of Group-invariant Orientation Parameters](http://arxiv.org/abs/1503.04474)" *International Conference on Information Fusion, 2015

### Project Description ###

The project is composed of 6 folders described as the following:

**Testcases**: The folder contain the c++ code examples of calling the implemented functions.

**MATLAB**: The folder contains the MATLAB code of the implemented functions and their testcases. 

**TransformToRef**: The folder contains the function TransformToRef that takes a reference quaternion direction and N quaternions as the inputs. It will transform the samples to their symmetric equivalent quaternions which point toward the reference direction. The symmetric operators are passed as argument Pm.

**TransformToFZ_cubic**: The folder contains the function TransformToFZ_cubic that transforms N quaternions into the fundamental zone (in cubic symmetry).

**EMmVMF**: The folder contains the function "EMmVMF" that takes a Nx4 double matrix as the input. The matrix is just the collection of N quaternions and the function will return the estimated mean and kappa of the N quaternions considering the cubic symmetry and the von Mises-Fisher model. If the number of clusters is specified to be larger than 1, the function will perform the clustering and return the estimated parameters for each of the clusters.

**EMmWatson**: The folder contains the function "EMmWatson" that takes a Nx4 double matrix as the input. The matrix is just the collection of N quaternions and the function will return the estimated mean and kappa of the N quaternions considering the cubic symmetry and the Watson model. If the number of clusters is specified to be larger than 1, the function will perform the clustering and return the estimated parameters for each of the clusters.

### How to compile the code ###

The Makefile of this project can be generated through [CMake](http://www.cmake.org/). Please follow the normal usage of CMake to generate the Makefile from the CMakelist.txt. Compiled binary includes the executable files for each of testcases and the dynamic library for each of the implemented function.

Note that the [Boost C++ Library](http://www.boost.org/) is required to compile this project.

### How to run the examples ###
We have provided the shell script "ExecuteTestCases.sh" for running the testcases. Before running the shell script, please specify the build path (**BIN_PATH**) of this project in the shell script.

Simply type:

```
#!shell

./ExecuteTestCases.sh
```

will run every testcases and dump the output to the log files. Or you can specify the name of the testcase which you want to run:

```
#!shell

./ExecuteTestCases.sh TransformToRef
```


### Contact Info ###

The repository is created and owned by **Yu-Hui Chen** @ University of Michigan from professor [Alfred Hero](http://web.eecs.umich.edu/~hero/)'s group.

If you have any question about the code, please contact me through:
*yuhuichen1015@gmail.com*

More information could be find on our lab's [wiki page](http://tbayes.eecs.umich.edu/start).